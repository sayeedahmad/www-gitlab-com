---
'19.0':
- Running a single database is deprecated
- Single database connection is deprecated
'18.0':
- Remove `previousStageJobsOrNeeds` from GraphQL
- Compliance pipelines
- OpenTofu CI/CD template
- CodeClimate-based Code Quality scanning will be removed
- Rename options to skip GitGuardian secret detection
- "`workflow:rules` templates"
- Replace `threshold` with `maxretries` for container registry notifications
- "`require_password_to_approve` field"
- The `direction` GraphQL argument for `ciJobTokenScopeRemoveProject` is deprecated
- Deprecate license metadata format V1
- Deprecate Terraform CI/CD templates
- Deprecate License Scanning CI/CD artifact report type
- 'Dependency Proxy: Access tokens to have additional scope checks'
- Breaking change to the Maven repository group permissions
- Default CI/CD job token (`CI_JOB_TOKEN`) scope changed
- The `ci_job_token_scope_enabled` projects API attribute is deprecated
- "`GITLAB_SHARED_RUNNERS_REGISTRATION_TOKEN` is deprecated"
- Behavior change for protected variables and multi-project pipelines
- List container registry repository tags API endpoint pagination
- "`sidekiq` delivery method for `incoming_email` and `service_desk_email` is deprecated"
- The GitLab legacy requirement IID is deprecated in favor of work item IID
- The `Project.services` GraphQL field is deprecated
- GitLab Runner platforms and setup instructions in GraphQL API
- Slack notifications integration
- ZenTao integration
- Support for REST API endpoints that reset runner registration tokens
- Registration tokens and server-side runner arguments in `gitlab-runner register`
  command
- Registration tokens and server-side runner arguments in `POST /api/v4/runners` endpoint
- GitLab Runner registration token in Runner Operator
- "`runnerRegistrationToken` parameter for GitLab Runner Helm Chart"
- Runner `active` GraphQL fields replaced by `paused`
- Self-managed certificate-based integration with Kubernetes
- Toggle notes confidentiality on APIs
'17.0':
- GraphQL API access through unsupported methods
- Upgrading the operating system version of GitLab SaaS runners on Linux
- Secure analyzers major version update
- Agent for Kubernetes option `ca-cert-file` renamed
- SAST analyzer coverage changing in GitLab 17.0
- npm package uploads now occur asynchronously
- "'repository_download_operation' audit event type for public projects"
- Deprecating Windows Server 2019 in favor of 2022
- Scan execution policies using `_EXCLUDED_ANALYZERS` variable override project variables
- Autogenerated Markdown anchor links with dash (`-`) characters
- Deprecate `version` field in feature flag API
- Removal of tags from small SaaS runners on Linux
- Deprecate `fmt` job in Terraform Module CI/CD template
- Support for self-hosted Sentry versions 21.4.1 and earlier
- Deprecate Python 3.9 in Dependency Scanning and License Scanning
- Min concurrency and max concurrency in Sidekiq options
- Maven versions below 3.8.8 support in Dependency Scanning and License Scanning
- Security policy field `match_on_inclusion` is deprecated
- Deprecate Grype scanner for Container Scanning
- Deprecate custom role creation for group owners on self-managed
- Dependency Scanning incorrect SBOM metadata properties
- "`dependency_files` is deprecated"
- Deprecate License Scanning CI templates
- "`omniauth-azure-oauth2` gem is deprecated"
- Compliance framework in general settings
- Heroku image upgrade in Auto DevOps build
- "`after_script` keyword will run for cancelled jobs"
- "`metric` filter and `value` field for DORA API"
- Support for setting custom schema for backup is deprecated
- License Scanning support for sbt 1.0.X
- License List is deprecated
- Dependency Scanning support for sbt 1.0.X
- GitLab Runner provenance metadata SLSA v0.2 statement
- JWT `/-/jwks` instance endpoint is deprecated
- List repository directories Rake task
- Proxy-based DAST deprecated
- 'GraphQL: deprecate support for `canDestroy` and `canDelete`'
- File type variable expansion fixed in downstream pipelines
- Legacy Geo Prometheus metrics
- The GitHub importer Rake task
- Container registry support for the Swift and OSS storage drivers
- Offset pagination for `/users` REST API endpoint is deprecated
- Security policy field `newly_detected` is deprecated
- Internal container registry API tag deletion endpoint
- "`postgres_exporter['per_table_stats']` configuration setting"
- Deprecate field `hasSolutions` from GraphQL VulnerabilityType
- Deprecate change vulnerability status from the Developer role
- 'Geo: Legacy replication details routes for designs and projects deprecated'
- GraphQL field `registrySizeEstimated` has been deprecated
- GraphQL field `totalWeight` is deprecated
- Twitter OmniAuth login option is deprecated from self-managed GitLab
- OmniAuth Facebook is deprecated
- Deprecated parameters related to custom text in the sign-in page
- Deprecate `CiRunner` GraphQL fields duplicated in `CiRunnerManager`
- The pull-based deployment features of the GitLab agent for Kubernetes is deprecated
- "`omnibus_gitconfig` configuration item is deprecated"
- Deprecate `message` field from Vulnerability Management features
- Duplicate storages in Gitaly configuration
- Deprecate Windows CMD in GitLab Runner
- Unified approval rules are deprecated
- GraphQL deprecation of `dependencyProxyTotalSizeInBytes` field
- GraphQL type, `RunnerMembershipFilter` renamed to `CiRunnerMembershipFilter`
- PostgreSQL 13 no longer supported
- Trigger jobs can mirror downstream pipeline status exactly
- CiRunner.projects default sort is changing to `id_desc`
- Required Pipeline Configuration is deprecated
- Queue selector for running Sidekiq is deprecated
- Old versions of JSON web tokens are deprecated
- HashiCorp Vault integration will no longer use the `CI_JOB_JWT` CI/CD job token
  by default
- The Visual Reviews tool is deprecated
- Maintainer role providing the ability to change Package settings using GraphQL API
- GitLab Helm chart values `gitlab.kas.privateApi.tls.*` are deprecated
- Auto DevOps support for Herokuish is deprecated
- 'GraphQL: The `DISABLED_WITH_OVERRIDE` value for the `SharedRunnersSetting` enum
  is deprecated'
- The `gitlab-runner exec` command is deprecated
- DAST ZAP advanced configuration variables deprecation
- GraphQL field `confidential` changed to `internal` on notes
- Vulnerability confidence field
- DingTalk OmniAuth provider
- "`projectFingerprint` GraphQL field"
- GraphQL `networkPolicies` resource deprecated
- Package pipelines in API payload is paginated
'16.7':
- Shimo integration
- "`user_email_lookup_limit` API field"
'16.6':
- Job token allowlist covers public and internal projects
'16.5':
- 'Geo: Housekeeping Rake tasks'
'16.3':
- RSA key size limits
- Twitter OmniAuth login option is removed from GitLab.com
- Bundled Grafana deprecated and disabled
- License Compliance CI Template
'16.0':
- GitLab administrators must have permission to modify protected branches or tags
- Changing MobSF-based SAST analyzer behavior in multi-module Android projects
- Legacy URLs replaced or removed
- Secure scanning CI/CD templates will use new job `rules`
- Secure analyzers major version update
- SAST analyzer coverage changing in GitLab 16.0
- Support for Praefect custom metrics endpoint configuration
- Development dependencies reported for PHP and Python
- Managed Licenses API
- License-Check and the Policies tab on the License Compliance page
- Legacy Praefect configuration method
- Default CI/CD job token (`CI_JOB_TOKEN`) scope changed
- External field in Releases and Release Links APIs
- Embedding Grafana panels in Markdown is deprecated
- Option to delete projects immediately is deprecated from deletion protection settings
- Environment search query requires at least three characters
- CI/CD jobs will fail when no secret is returned from Hashicorp Vault
- External field in GraphQL ReleaseAssetLink type
- Deprecation and planned removal for `CI_PRE_CLONE_SCRIPT` variable on GitLab SaaS
- Enforced validation of CI/CD parameter character lengths
- Developer role providing the ability to import projects to a group
- Use of third party container registries is deprecated
- The latest Terraform templates will overwrite current stable templates
- The API no longer returns revoked tokens for the agent for Kubernetes
- "`environment_tier` parameter for DORA API"
- Non-standard default Redis ports are deprecated
- Configuring Redis config file paths using environment variables is deprecated
- Projects API field `operations_access_level` is deprecated
- Container registry pull-through cache
- Cookie authorization in the GitLab for Jira Cloud app
- Deployment API returns error when `updated_at` and `updated_at` are not used together
- Limit personal access token and deploy token's access with external authorization
- Dependency Scanning support for Java 13, 14, 15, and 16
- Auto DevOps no longer provisions a PostgreSQL database by default
- DAST report variables deprecation
- Support for periods (`.`) in Terraform state names might break existing states
- Conan project-level search endpoint returns project-specific results
- "`POST ci/lint` API endpoint deprecated"
- Azure Storage Driver defaults to the correct root prefix
- The Phabricator task importer is deprecated
- KAS Metrics Port in GitLab Helm Chart
- DAST API scans using DAST template is deprecated
- DAST API variables
- Jira DVCS connector for Jira Cloud
- Configuration fields in GitLab Runner Helm Chart
- "`vulnerabilityFindingDismiss` GraphQL mutation"
- Starboard directive in the configuration of the GitLab agent for Kubernetes
- Non-expiring access tokens
- Toggle behavior of `/draft` quick action in merge requests
- Container Scanning variables that reference Docker
- CAS OmniAuth provider
- Use of `id` field in `vulnerabilityFindingDismiss` mutation
- Security report schemas version 14.x.x
- Redis 5 deprecated
- Remove `job_age` parameter from `POST /jobs/request` Runner endpoint
- Major bundled Helm Chart updates for the GitLab Helm Chart
- Bundled Grafana Helm Chart is deprecated
- Deprecated Consul http metrics
- Work items path with global ID at the end of the path is deprecated
- Legacy Gitaly configuration method
- "`name` field for `PipelineSecurityReportFinding` GraphQL type"
- PostgreSQL 12 deprecated
- Monitor performance metrics through Prometheus
- GitLab self-monitoring project
- "`started` iteration state"
- Deprecate legacy Gitaly configuration methods
- "`CI_BUILD_*` predefined variables"
- GraphQL API Runner status will not return `paused`
- Changing merge request approvals with the `/approvals` API endpoint
'15.9':
- "`omniauth-authentiq` gem no longer available"
- Live Preview no longer available in the Web IDE
- SaaS certificate-based integration with Kubernetes
'15.7':
- File Type variable expansion in `.gitlab-ci.yml`
'15.4':
- SAST analyzer consolidation and CI/CD template changes
'15.10':
- Automatic backup upload using Openstack Swift and Rackspace APIs
'15.0':
- Querying usage trends via the `instanceStatisticsMeasurements` GraphQL node
- Logging in GitLab
- OAuth implicit grant
- OAuth tokens without expiration
- Tracing in GitLab
- GraphQL permissions change for Package settings
- Test coverage project CI/CD setting
- htpasswd Authentication for the container registry
- Background upload for object storage
- Secure and Protect analyzer major version update
- Secure and Protect analyzer images published in new location
- Dependency Scanning Python 3.9 and 3.6 image deprecation
- Request profiling
- Out-of-the-box SAST support for Java 8
- SAST support for .NET 2.1
- Deprecate feature flag PUSH_RULES_SUPERSEDE_CODE_OWNERS
- Vulnerability Check
- Container Network and Host Security
- Support for gRPC-aware proxy deployed between Gitaly and rest of GitLab
- GraphQL ID and GlobalID compatibility
- Optional enforcement of PAT expiration
- Optional enforcement of SSH expiration
- Retire-JS Dependency Scanning tool
- External status check API breaking changes
- Sidekiq metrics and health checks configuration
- "`projectFingerprint` in `PipelineSecurityReportFinding` GraphQL"
- Required pipeline configurations in Premium tier
- Elasticsearch 6.8
- "`apiFuzzingCiConfigurationCreate` GraphQL mutation"
- "`promote-to-primary-node` command from `gitlab-ctl`"
- "`artifacts:reports:cobertura` keyword"
- Legacy approval status names from License Compliance API
- CI/CD job name length limit
- bundler-audit Dependency Scanning tool
- "`type` and `types` keyword in CI/CD configuration"
- "`pipelines` field from the `version` field"
- "`promote-db` command from `gitlab-ctl`"
- "`dependency_proxy_for_private_groups` feature flag"
- Value Stream Analytics filtering calculation change
- Known host required for GitLab Runner SSH executor
- Update to the container registry group-level API
- Legacy database configuration
- "`Versions` on base `PackageType`"
- Changing an instance (shared) runner to a project (specific) runner
- Support for SLES 12 SP2
- "`defaultMergeCommitMessageWithDescription` GraphQL API field"
- GitLab Serverless
- Dependency Scanning default Java version changed to 17
- OmniAuth Kerberos gem
- Audit events for repository push events
- Outdated indices of Advanced Search migrations
'14.9':
- Integrated error tracking disabled by default
'14.10':
- Permissions change for downloading Composer dependencies
