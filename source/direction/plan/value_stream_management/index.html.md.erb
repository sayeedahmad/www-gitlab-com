---
layout: markdown_page
title: "Category Direction - Value Stream Management"
description: "Value stream management helps software organizations understand and measure how quickly and reliably they can deliver value to end users."
---

- TOC
{:toc}

| Category | **Value Stream Management** |
| --- | --- |
| Stage | [Plan](https://about.gitlab.com/handbook/product/categories/#plan-stage) |
| Group | [Optimize](https://about.gitlab.com/handbook/product/categories/#optimize-group) |
| Maturity | [Viable](/direction/maturity/) |
| Content Last Reviewed | `2024-07-14` |

Thanks for visiting the Value Stream Management direction page. This page is actively maintained by the [Optimize group](https://about.gitlab.com/handbook/product/categories/#optimize-group).  

You can contribute or provide feedback by **sharing your feedback about your Value Stream Management experience in this [survey](https://gitlab.fra1.qualtrics.com/jfe/form/SV_50guMGNU2HhLeT4)**, or posting questions and comments in the [public epic](https://gitlab.com/groups/gitlab-org/-/epics/9882).

### Overview

<%= partial("direction/plan/value_stream_management/templates/overview") %>

### Vision

GitLab will be the tool of choice for the data-driven DevOps organization — enabling teams and managers to understand all aspects of productivity, quality, security and delivery without any complex configurations or data scientists.


### 1 year plan 


1. Demonstrate the value of GitLab AI features, by adding ["AI Impact" analytics to the Value Stream Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/12978) and [Value stream forecasting](https://gitlab.com/gitlab-org/gitlab/-/issues/407798).

2. Expanded [Value Stream Dashboard](https://www.youtube.com/watch?v=8pLEucNUlWI) to enables organizations with a [comprehensive overview](https://youtu.be/yNZRac7gyYo) of the value they deliver to customers and [optimize their software development processes](https://about.gitlab.com/blog/2023/06/26/three-steps-to-optimize-software-value-streams/) to deliver better software faster.
To achieve this, we will focus on:

   * Complete the "Value Cycle" with united view of [Value Stream and Product Analytics metrics](https://gitlab.com/groups/gitlab-org/-/epics/8925). 
   * Extend [VSA & DORA Integrations](https://gitlab.com/groups/gitlab-org/-/epics/8369).
   * Enable fast analytical queries and deep data analysis by replicating GitLab analytics metrics into a [new data warehouse](https://gitlab.com/groups/gitlab-org/-/epics/9318).

3. Enhance usability: Deliver excellent "out-of-the-box" [experience](https://about.gitlab.com/direction/#world-class-devsecops-experience) for ~"Category:Value Stream Management" to visualize for everyone the unique power of measuring software delivery value in a single DevSecOps platform. To achieve this, we will focus on:

   * Uplift [VSA usability](https://gitlab.com/groups/gitlab-org/-/epics/5272) to enabling teams and managers understand all aspects of productivity, quality, security and delivery without any complex configurations or data scientists.
   * Adding a new [VSA settings page](https://gitlab.com/groups/gitlab-org/-/epics/9129) to improve the persistency 
   * Usability improvements for ["tasks by type" chart](https://gitlab.com/groups/gitlab-org/-/epics/7412).
   * ~Dogfooding [VSD, VSA and DORA](https://gitlab.com/groups/gitlab-org/-/epics/3894).

4. Leverage GitLab Duo suite AI capabilities for boosting productivity when using VSM and DORA.To achieve this, we will focus on:

   * Introduce [Value Streams forecasting to VSD comparison panel](https://gitlab.com/gitlab-org/gitlab/-/issues/407798).
   * Adding VSM "Duo Chat Analytics" - [adding analytics tool to GitLab Duo Chat](https://gitlab.com/gitlab-org/gitlab/-/issues/426861).
   * Adding [Reports Generation for Value Stream Dashboard (Scheduled Reports)](https://gitlab.com/groups/gitlab-org/-/epics/10880)

#### What's next and why

<%= partial("direction/plan/value_stream_management/templates/next") %>

#### Recent accomplishments

- Adding the first phase of the ["AI Impact" analytics to the Value Stream Dashboard](https://about.gitlab.com/releases/2024/05/16/gitlab-17-0-released/#ai-impact-analytics-in-the-value-streams-dashboard). Using this MVC users can observe how changes in "Code Suggestions usage rate" metric correlate with changes in others software development life cycle (SDLC) metrics. 

- Adding a new [usage overview panel in the Value Streams Dashboard](https://about.gitlab.com/releases/2024/05/16/gitlab-17-0-released/#new-usage-overview-panel-in-the-value-streams-dashboard). This new visualization gives a clear picture of GitLab usage in the context of software development life cycle (SDLC).

- Introducing a new [median time to merge metric](https://about.gitlab.com/releases/2024/05/16/gitlab-17-0-released/#new-median-time-to-merge-metric-in-value-streams-dashboard) and a [new "Contributor Count" metric in the Value Streams dashboard](https://about.gitlab.com/releases/2024/03/21/gitlab-16-10-released/#new-contributor-count-metric-in-the-value-streams-dashboard), to enable software leaders to gain insights into the relationship between team velocity, DevOps performance, software stability, security exposures, and team productivity.

- Simplified the [configuration file schema for Value Streams Dashboard](https://about.gitlab.com/releases/2024/05/16/gitlab-17-0-released/#simplified-configuration-file-schema-for-value-streams-dashboard). In the new format, the fields provide more flexibility of displaying the data and laying out the dashboard panels. With the new framework, administrators can track changes to the dashboard over time.

Simplified configuration file schema for Value Streams Dashboard
- Introducing the [New ClickHouse-Based Contribution Analytics](https://about.gitlab.com/releases/2024/03/21/gitlab-16-10-released/#new-clickhouse-integration-for-high-performance-devops-analytics) and the Contribution Analytics on GitLab.com will now run through the ClickHouse Cloud cluster.

- Adding filters inherit to the link between [VSA "Lead time" and "Issue Analytics"](https://about.gitlab.com/releases/2024/03/21/gitlab-16-10-released/#new-clickhouse-integration-for-high-performance-devops-analytics). Value stream analytics now applies the same filters when drilling down from the Lead time tile to the Issue Analytics report. 

- [New stage events -  iteration event](https://about.gitlab.com/releases/2024/02/15/gitlab-16-9-released/#new-stage-events-for-custom-value-stream-analytics). This improve the tracking of development workflows in GitLab, we added the Value Stream Analytics has been extended with a new stage event: "Issue first associated with an iteration".

- Adding new metric - ["Issues closed" to the "Issues Analytics" report](https://about.gitlab.com/releases/2024/01/18/gitlab-16-8-released/#deeper-insights-into-velocity-in-the-issue-analytics-report) to help software leaders to track the total number of resolved issues over a specific period. With this addition, Gitlab users can now gain insights into trends associated with their projects and improve the overall turn-around time and value delivered to their customers.

- Introduce group-level [landing page for "Analytics Dashboard" menu item](https://about.gitlab.com/releases/2024/01/18/gitlab-16-8-released/#introduce-group-level-landing-page-for-analytics-dashboards). This enhancement ensures a more consistent and user-friendly navigation experience to the [Value Streams Dashboard](https://youtu.be/8pLEucNUlWI?feature=shared). 

- [New drill-down view from Insights report charts](https://about.gitlab.com/releases/2023/12/21/gitlab-16-7-released/#new-drill-down-view-from-insights-report-charts). New drill-down capability added to the [Insights reports](https://docs.gitlab.com/ee/user/project/insights/) allows you to drill down to the Issue analytics report for deeper analysis.

- Adding [predefined date range for value stream analytics](https://gitlab.com/gitlab-org/gitlab/-/issues/408656) - making it more efficient and user-friendly to understand [where time is spent during the development lifecycle](https://about.gitlab.com/blog/2023/06/01/value-stream-total-time-chart/). 

- Releasing the [Value Stream Dashboard](https://about.gitlab.com/releases/2023/05/22/gitlab-16-0-released/#value-streams-dashboard-is-now-generally-available) - this first release is focused on measuring software development (DORA4), understanding security exposure and the flow of value delivery (Value Stream Analytics) across projects and groups.

<iframe width="560" height="315" src="https://www.youtube.com/embed/8pLEucNUlWI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Value Stream Dashboard - adding [Vulnerabilities metrics to the "Metrics comparison panel"](https://gitlab.com/gitlab-org/gitlab/-/issues/383697).
- Value Stream Dashboard - enhanced with new metrics: [Merge request (MR) throughput and Total closed issues (Velocity)](https://about.gitlab.com/releases/2023/08/22/gitlab-16-3-released/#new-velocity-metrics-in-the-value-streams-dashboard).  
- New [Value Stream Analytics total time chart](https://about.gitlab.com/releases/2023/04/22/gitlab-15-11-released/#new-visualization-of-stages-breakdown-in-value-stream-analytics) to visualization the stages breakdown - This new visualization helps managers uncover how long it actually takes to complete the development process from idea to production,  as well as how much time the team spend in each stage of the workflow. 
Check out [this blog post](https://about.gitlab.com/blog/2023/06/01/value-stream-total-time-chart/) to learn more about the new chart. 


#### Top Vision Item(s)

- Software delivery [optimization AI recommendation](https://gitlab.com/groups/gitlab-org/-/epics/10329) 
- Enhance VSM for agile planning - [tracking the new Workflow Status](https://gitlab.com/gitlab-org/gitlab/-/issues/466409)
- [AI Descriptive Analytics with quick insights](https://gitlab.com/gitlab-org/gitlab/-/issues/389233)
- Adding [business value metrics](https://gitlab.com/gitlab-org/gitlab/-/issues/381730) widget into the Value Streams Dashboard.
- Measuring [SPACE framework metrics in GitLab](https://gitlab.com/gitlab-org/gitlab/-/issues/372054)

- Value-stream Autonomous mapping tool

### Top cross-stage items

- Value Stream Dashboard - extend to "Product Analytics" [schema-driven customizable UI](https://gitlab.com/groups/gitlab-org/-/epics/8925).
- DORA integration with GitLab [Incident Tag.](https://gitlab.com/gitlab-org/gitlab/-/issues/336026)
- Value Stream Dashboard - [Scheduled Reports / Reports Generation with the CI/CD catalog](https://gitlab.com/gitlab-org/gitlab/-/issues/416239).

## Competitive Analysis

### Why using GitLab for Value Stream Management?

1. Out-of-the-box [Value Stream Insight, Reporting and dashboards](https://gitlab.com/gitlab-org/gitlab/-/value_stream_analytics?created_after=2022-12-31&created_before=2023-01-29&stage_id=issue&sort=end_event&direction=desc&page=1).
2. Collects and shows data across the entire software lifecycle with no integrations to be managed.
3. [DORA metrics](https://about.gitlab.com/direction/plan/dora_metrics/) coupled with the Value Stream Analytics that helps draw insights about the velocity and stability of software delivery lifecycle.
4. A common data model representing people, processes, work items, and time to remove silos.
5. Multiple [custom value streams](https://docs.gitlab.com/ee/user/group/value_stream_analytics/#create-a-value-stream-with-custom-stages) for end-to-end view of value delivery and application health
6. The One DevOps Platform - Single Application for Entire DevOps Lifecycle
7. Open Source; Everyone Can Contribute. 
8. Deploy Your Software Anywhere.

### Competitive Landscape

Top 3 Competitors: 

1. [Tasktop](https://www.tasktop.com/) (acquired by Planview) - Best In Class (BIC) competitor.
2. [Digital.ai](https://digital.ai/)
3. [Plutora Analytics](https://www.plutora.com/platform/plutora-analytics) 

Based on our analysis,  we've identified Planview-Tasktop as the Best In Class (BIC) competitor over Digital.ai and Plutora.

#### Tasktop - Best In Class (BIC) competitor
[Tasktop ](https://www.tasktop.com/integration-hub) is exclusively focused on Value Stream Management and allows users to connect more than 50 tools together, including Atlassian's JIRA, GitLab, GitHub, JamaSoftware, CollabNet VersionOne, Xebia Labs, and TargetProcess to name a few. Tasktop serves as an integration layer on top of all the software development tools that a team uses and allows for mapping of processes and people in order to achieve a common data model across the toolchain. End users can visualize the flows between the different tools and the data can be exported to a database for visualization through BI tools.

Based on our analysis, we've identified these gaps against Tasktop:

| Gap | Roadmap to close this gap|
|-----|---------|
| Limited Analytics & Visualization capabilities | [Adding Value Stream Dashboard - Comparison page](https://gitlab.com/groups/gitlab-org/-/epics/9559),  [Overview page](https://gitlab.com/groups/gitlab-org/-/epics/9558),  [Schema-driven customizable UI](https://gitlab.com/groups/gitlab-org/-/epics/8925) |
| limited flow analysis | [Add VSA Overview cumulative flow diagram](https://gitlab.com/gitlab-org/gitlab/-/issues/366780),  [Add Stage time - scatterplot](https://gitlab.com/groups/gitlab-org/-/epics/8374),  [Add SAFe Flow Metrcis](https://gitlab.com/groups/gitlab-org/-/epics/6795),  [Usability improvements to "tasks by type" chart](https://gitlab.com/groups/gitlab-org/-/epics/7412) |
| limited number of integrations | [VSA API](https://gitlab.com/groups/gitlab-org/-/epics/8369) |
| Missing value delivery metrics | [Adding business value metrics](https://gitlab.com/gitlab-org/gitlab/-/issues/381730),  [Adding OKR Health status](https://gitlab.com/gitlab-org/gitlab/-/issues/384923) |

#### Digital.ai 

[Digital.ai](https://digital.ai/) AI-Powered DevOps platform.  Digital.ai has been on a multiyear, multiacquisition journey that includes Arxan, CollabNet
VersionOne, Experitest, Numerify, and XebiaLabs. Its plan to be a front-runner in AI-driven software delivery for Global 5000 enterprises.

[XebiaLabs' analytics (acquired by Digital.ai)](https://docs.xebialabs.com/xl-release/concept/release-dashboard-tiles.html) are predominantly focused on the Release Manager and give useful overviews of deployments, issue throughput and stages. The company integrates with JIRA, Jenkins, etc and end users can see in which stage of the release process they are.

#### Plutora
[Plutora Analytics](https://www.plutora.com/platform/plutora-analytics) Plutora is a privately held global software (SaaS) company, providing Value Stream Management solutions for enterprise IT in the areas of Release Management and Orchestration, Test Environment Management, Deployment Management, and Analytics.. Plutora seem to target mainly the release managers with their [Time to Value Dashboard](https://www.plutora.com/platform/time-to-value-dashboard). The company also integrates with JIRA, Jenkins, GitLab, CollabNet VersionOne, etc but there is still a lot of configuration that seems to be left to the user.

Key Features for Comparison:

1. Data Analytics - Visualize the data into valuable insights and actionable information to support decision-making and strategic planning. The ability to analyze the stream performance, team productivity, and work distribution. This includes custom dashboards and detailed reports with the underline data.   
2. Data Collection - Capturing and aggregating DORA, Flow, DevOps, Ops and Security metrics.
3. Common Data Model - Set of value stream objects representing the DevOps lifecycle to enable customers to map the end-to-end processes. The ability to create and manage single or multiple value streams as a single source of insight for the organization.This include - users, processes (workflow labels), work items (issues, MRs and epics) and time tracking. 
4. Integrations - Inbound and outbound integrations with 3rd party tools.
5. Value Measurement - Connect software investments to business results. Capture, calculate, and track product usage, OKRs, cost metrics, and business KPIs (Revenue, Renewals).
6. Governance and Compliance - The ability to monitor compliance to organizational standards.
7. AI/ML - Leverages AI/ML for better decision-making. The ability to predict trends, detects blind spots, bobble-up insights, and automate processes to reduce manual tasks.

GitLab vs the Top 3 Competitors - Planview-Tasktop/Digital.ai/Plutora:

1. GitLab versus [Planview-Tasktop](https://www.tasktop.com/): 

| Feature | GitLab | Planview-Tasktop |
| ------ | ------ | ------ |
|   Data Analytics   | 🟨  |  🟩 |
|   Capturing Data, Metrics and KPIs    | 🟨 | 🟩 |
|   Common Data Model    | 🟩 | 🟩 |
|   Integrations     | ⬜️  | 🟩 |
|   Value Measurement    | ⬜️  | 🟩 |
|   Governance and Compliance    | 🟩 | 🟩 |
|   AI/ML     | ⬜️ | 🟨 |

2. GitLab versus [Digital.ai](https://digital.ai/): 

| Feature | GitLab | Digital.ai |
| ------ | ------ | ------ |
|   Data Analytics   | 🟨  |  🟩 |
|   Capturing Data, Metrics and KPIs    | 🟨| 🟩 |
|   Common Data Model    | 🟩 | 🟩 |
|   Integrations     | ⬜️  | 🟩 |
|   Value Measurement    | ⬜️  | 🟩 |
|   Governance and Compliance    | 🟩 | 🟨 |
|   AI/ML     | ⬜️ | 🟩 |

3. GitLab versus [Plutora](https://www.plutora.com): 

| Feature | GitLab | Plutora |
| ------ | ------ | ------ |
|   Data Analytics   | 🟨  |  🟩 |
|   Capturing Data, Metrics and KPIs    | 🟨| 🟩 |
|   Common Data Model    | 🟩 | 🟩 |
|   Integrations     | ⬜️  | 🟩 |
|   Value Measurement    | ⬜️  | 🟨 |
|   Governance and Compliance   | 🟩 | 🟨 |
|   AI/ML     | ⬜️ | ⬜️ |

More competitors in our Landscape:

#### TargetProcess
[Targetprocess](https://www.targetprocess.com) tries to provide a full overview of the delivery process and integrates with Jenkins, GitHub and JIRA. The company also provides customizable dashboards that can give an overview over the process from ideation to delivery.

#### GitPrime
Although [GitPrime](https://www.gitprime.com) doesn't try to provide a value stream management solution, it focuses on productivity metrics and cycle time by looking at the productivity of a team. It exclusively uses only git data.

#### Azure DevOps
Naturally, [Azure](https://docs.microsoft.com/en-us/azure/devops/report/dashboards/analytics-widgets?view=azure-devops) is working on adding analytics that can help engineering teams become more effective but it's still in very early stages. It has also recently acquired [PullPanda](https://pullpanda.com).

#### Velocity by Code Climate
Similarly to GitPrime, [Code Climate](https://codeclimate.com/velocity/understand-diagnose/) focuses on the team and uses git data only.

#### Gitalytics
Similarly to GitPrime, [Gitalytics](https://gitalytics.com) focuses on the team and uses git data only.

#### Gitential
[Gitential](https://gitential.com)

#### Gitclear
[Gitclear](https://gitclear.com)

#### Gitlean
[Gitlean](https://gitlean.com)

#### CollabNet VersionOne
[CollabNet VersionOne](https://www.collab.net) provides users with the ability to input a lot of information, which is a double edged sword as it can lead to duplication of effort and stale information when feeds are not automated. It does however, allow a company to visualize project streams from a top level with all their dependencies. End users can also create customizable reports and dashboards that can be shared with senior management.

#### CA Technologies
[CA Agile Central](https://docs.ca.com/en-us/ca-agile-central/saas/iteration-burndown) combines data across the planning process in a single integrated page with custom applications available to CA Agile Central users. The applications can be installed in custom pages within CA Agile Central or on a dashboard.

#### Atlassian's JIRA Align
[Agile Craft](https://agilecraft.com)

#### Sleuth
[Sleuth](https://www.sleuth.io)

#### Allstacks

[allstacks.com](https://www.allstacks.com/product/value-stream-intelligence)

#### Blueoptima

[blueoptima.com](https://www.blueoptima.com/about/)

### Analyst Landscape

[Gartner Market Guide for DevOps Value Stream Delivery Platforms](https://about.gitlab.com/analysts/gartner-vsdp21/). It is also possible to utilize GitLab's Value Stream Management as a [Software Engineering Intelligence Platform](https://www.gartner.com/en/documents/4214399).

Forrester's New Wave: Value Stream Management Tools, Q3 2018 uncovered an emerging market with no leaders. However, vendors from different niches of the development pipeline are converging to value stream management in response to customers seeking greater transparency into their processes.

Forrester’s vision for VSM includes:
- end-to-end visibility of the software development process, including the corresponding capture and storage of data, events, and artifacts
- definition and visualization of key performance indicators (KPIs)
- inclusive customer experience, which allows multiple roles (PMs, developers, QA, and release managers) to collaborate in one place
- governance, i.e. a framework to monitor compliance to organizational standards, automated audit capabilities and traceability.

Other Analysts have highlighted that Gitlab data gathering has much to offer and much more to mine and enable the insight generation. We have an immediate opportunity i to extend the insight generation based on the data gathered in the delivery pipelines. Once this is achieved we will integrate additional data sources beyond the DevOps toolchains.

We have the ability to reach the decision makers that are onsuming the insights generated from the Gitlab platform, and one of the key elements here is getting beyond the DORA 4 metrics into those that are more specifically targeted: security, compliance, financial, product, but also enterprise architecture, AI/ML delivery teams and the like.

Additional functionality, requested by clients includes:
- integration with other tools, including the ability to double-click into each tool to directly observe status or take action
- business value custom definitions in terms of financials, time, effort or similar
- mapping of business value, people, processes, data
- visualization dashboards, which users can customize to support different role-based views.

### Jobs to be done

<%= partial("direction/jtbd-list", locals: { stage_key: "Value Stream Management" }) %>

### Maturity plan

This category is currently **Viable**. Our next step is **Complete** ([see epic](https://gitlab.com/groups/gitlab-org/-/epics/9882#maturity-plan)). You can read more about GitLab's [maturity framework here](https://about.gitlab.com/direction/maturity/).
