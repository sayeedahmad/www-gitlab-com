---
layout: markdown_page
title: "GitLab Legal"
description: "This page lists the subprocessors that GitLab may use."
---
{::options parse_block_html="true" /}
## Sub-processors of GitLab

When acting as a processor for Customer, Gitlab may engage the following entities to carry out specific processing activities, including those processing activities necessary to deliver the Services under the GitLab subscription.

[Sign Up](#sign-up) to be notified of a change to the list.

### GitLab Affiliates

| Affiliate Entity |Location|Service Provided|
|---|---|---|
|GitLab BV|Netherlands|Support Services|
|GitLab IT BV|Netherlands|Support Services|
|GitLab UK Ltd|United Kingdom|Support Services|
|GitLab Ireland Ltd|Ireland|Support Services|
|GitLab GmbH|Germany|Support Services|
|GitLab France SAS|France|Support Services|
|GitLab Pty Ltd|Australia|Support Services|
|GitLab Canada Corp.|Canada|Support Services|
|GitLab GK|Japan|Support Services|
|GitLab South Korea|South Korea|Support Services|
|GitLab Singapore Holding PTE Ltd|Singapore|Support Services|
|GitLab Singapore PTE Ltd|Singapore|Support Services|

### Third Party Sub-processors

|Third Party Entity|Location|Service Provided|GitLab Product|
|---|---|---|---|
|Google LLC|United States|Cloud Hosting|GitLab.com and Dedicated|
|Google LLC|United States|[AI-Powered Product Features](https://docs.gitlab.com/ee/user/gitlab_duo/index.html)|GitLab.com, Dedicated, and Self-Managed|
|Amazon Web Services Inc.|United States* <br>*_For GitLab Dedicated Customers the hosting location is your choice from our [available AWS regions](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/#available-aws-regions)_|Cloud Hosting|GitLab.com and Dedicated|
|Zendesk Inc.|United States and Germany|Support Services|GitLab.com, Dedicated, and Self-Managed|
|Elasticsearch Inc.|United States|Search Functionality and Application Logging and Debugging|GitLab.com, Dedicated, and Self-Managed|
|Cloudflare Inc.|United States and European Union|Content Delivery Network|GitLab.com|
|Mailgun Technologies, Inc.|United States and European Union|Transactional Email APIs|GitLab.com|
|Anthropic, PBC|United States|[AI-Powered Product Features](https://docs.gitlab.com/ee/user/gitlab_duo/index.html)|GitLab.com, Dedicated, and Self-Managed|
|ClickHouse, Inc.|United States|Database Hosting|GitLab.com|
|Unbabel Inc.|United States and Portugal|Customer Support Translations|GitLab.com, Dedicated, and Self-Managed|
|FRAME Technology, Inc.|United States|Automated Support Ticket Analysis|GitLab.com, Dedicated, and Self-Managed|
|IntouchCX Disrupt Inc.|India|Outsourced Customer Support Business Processes|GitLab.com, Dedicated, and Self-Managed|

### Professional Services Sub-processors
_These Sub-processors only apply in the event Customer has entered into a [Professional Services Agreement](https://about.gitlab.com/handbook/legal/professional-services-agreement/) with GitLab._

|Third Party Entity|Region|Service Provided|
|---|---|---|
|Sirius Federal|United States|Professional Services|
|CPrime|United States|Professional Services|
|D Ops|United States and United Kingdom|Professional Services|
|Adfinis|European Economic Area and Australia|Professional Services|
|River Point Technology|United States|Professional Services|
|Adaptavist|United Kingdom|Professional Services|
|ReleaseTEAM|United States and Canada|Professional Services|
|Eficode|United States and European Economic Area|Professional Services|
|Stratus Midco d/b/a Contegix|United States and Canada|Professional Services|
|Black Diamond Development, LLC|United States|Professional Services|
|Cloudfresh Central Europe s.ro.|European Economic Area, Africa, and Middle East|Professional Services|

### Sign Up
Complete this form to be notified of changes to our sub-processors.

<script src="//page.gitlab.com/js/forms2/js/forms2.min.js"></script>

<form id="mktoForm_2833"></form>

<script>
  var formAfterSuccessDo = function()
  {
    $('.confirmform').attr('style', 'visibility: visible');
    $('.confirmform').attr('style', 'height: initial');
    $('html, body').animate({scrollTop: parseInt($('#confirmform').offset().top-100)}, 500);
  };
  MktoForms2.setOptions(
  {
    formXDPath : "/rs/194-VVC-221/images/marketo-xdframe-relative.html"
  });
  MktoForms2.loadForm("//page.gitlab.com", "194-VVC-221", 2833, function(form)
  {
    form.onSuccess(function()
    {
      form.getFormElem().hide();
      formAfterSuccessDo();
      return false;
    });
  });
</script>

<div class="confirmform" style="display:none;">
  <h3>Submission received</h3>
  <p>Thank you for signing up to receive updates to our subprocessor list.</p>
</div>

{::options parse_block_html="false" /}

