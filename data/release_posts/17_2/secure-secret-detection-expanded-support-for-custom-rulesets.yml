---
features:
  primary:
  - name: 'Expanded support of custom rulesets in pipeline secret detection'
    available_in: [ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/index.html#customize-analyzer-rulesets'
    image_url: '/images/17_2/secrets-expanded-custom-rulesets-support.png'
    reporter: smeadzinger
    stage: secure
    categories:
    - Secret Detection
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/336395'
    mr_url: 'https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/merge_requests/310'
    description: |
      We have expanded support of custom rulesets in pipeline secret detection.

      You can use two new types of passthroughs, `git` and `url`, to configure remote rulesets. This makes it easier to manage workflows such as sharing ruleset configurations across multiple projects.

      You can also extend the default configuration with a remote ruleset by using one of those new types of passthroughs.

      The analyzer also now supports:

      * Chaining up to 20 passthroughs into a single configuration to replace predefined rules.
      * Including environment variables in passthroughs.
      * Setting a timeout when loading a passthrough.
      * Validating TOML syntax in ruleset configuration.
