---
layout: markdown_page
title: Product Direction - Analytics Instrumentation
description: "Analytics Instrumentation manages a variety of technologies that are important for GitLab's understanding of how our users use our products. Learn more here!"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Vision

As part of our overall [3 year Strategy](https://about.gitlab.com/company/strategy/#three-year-strategy), GitLab is focused on [customer results](https://about.gitlab.com/company/strategy/#1-customer-results). In order to empower our customers to achieve their goals and to enable GitLab to ship a world class DevOps product, we must provide the necessary instrumentation interfaces so teams can identify opportunities, mitigate risks, and make the right decisions. This helps our users and us achieve our goals by enabling us to make data-driven decisions.

To accomplish this, we aim to leverage our deep expertise, tied with investments in a strong technical foundation, to enable our users and all teams within GitLab to instrument their applications so that they can then analyze and report on the data those applications create. Like many top SaaS software companies, we are moving to be a product-led and data-driven organization. Through a partnership with our Data Team and collaboration with Product and Engineering we are cultivating a strong data focused culture within GitLab and for our users.

In addition to using data to drive decisions within GitLab, ensuring customer results means we will provide the platform and tools for them to gather data about their own apps. This means they can also make data-driven decisions to improve their products and achieve their business goals.

## Guiding Principles

The key thesis of our group is that providing more visibility into how GitLab is used allows us to make better decisions which lead to better business outcomes for ourselves and our users. In order to build the best DevOps product we can and provide the most value for our customers, we need to collect and analyze usage data across the entire platform to investigate trends, patterns, and opportunities. Insights generated from our Analytics Instrumentation program enable GitLab to identify the best place to invest time and resources, which categories to push to maturity faster, where our UI experience can be improved, and how product changes effect the business.

We understand that usage tracking is a sensitive subject, and we respect and acknowledge our customers' concerns around what we track, how we track it, and what we use it for. We will always be transparent about what we track and how we track it. In line with our company's value of [transparency](https://about.gitlab.com/handbook/values/#transparency), and our [commitment to individual user privacy](https://about.gitlab.com/handbook/product/analytics-instrumentation-guide/service-usage-data-commitment/), our tracking source code and documentation will always be public.

As we build solutions for GitLab and our users to instrument their apps, aspects we will focus on are:

* Usability: We will ensure that it is straightforward to add instrumentation to relevant parts of the app across a wide range of use cases. If it is hard to add instrumentation, users will give up and not do it.
* Scalability: We will ensure the systems we build and provide are robust and suitable for production-grade loads. If users cannot trust the systems we provide to be usable and reliable, they will look for alternatives.
* Manageability: We will ensure that it is possible to easily understand what parts of the app are instrumented and what sort of data will be produced. If it is not clear what is already instrumented, people may not be aware of what their current data options are, re-implement the same event multiple times, or remove an event that someone else is using.
* Adaptability: GitLab's code base is ever evolving. Instrumentation code needs to adapt to changes in where or how a feature is triggered, to keep resulting events consistent as well as not get into the way of feature changes.

<iframe width="560" height="315" src="https://www.youtube.com/embed/_D-C8B5vPS0" frameborder="0" allowfullscreen>
</iframe>

*In this [GitLab Unfiltered video](https://youtu.be/_D-C8B5vPS0) Niko is talking about the groups's approach to Analytics Instrumentation tools improvements and evolution*

### Our opinion on automated tracking

It is a valid question to ask why analytics instrumentation can not be automated to a high extent by automatically tracking API hits based on the URL or button clicks in the UI.
While this would be the most usable solution, as it does not require any manual instrumentation, it is neither scalable nor manageable or adaptable:

* It would multiply the amount of events we receive, which would affect the effort needed for event processing. Querying the useful events will become slower and/or more costly due to the high amount of unneeded events. This can impact the reliability of the system as well.
* It increases the likelihood of personal data unintentionally leaking into event collection as API request parameters or button texts are collected.
* It is not clear which events are useful and which are not since there is no intent behind tracking a specific event. The same user interaction could be tracked through multiple automated events, like a UI click or an API hit.
* It does not obviate the need for instrumentation, since not all events can be automatically tracked, such as the results of an asynchronous computation.
* It creates a dependency between feature code and analytics code since automatic tracking by definition depends on implementation details such as the URI of an API call or the CSS selector of a button. This can lead to a resistance to feature changes since they could break tracking. Analytics instrumentation aims to benefit future feature development not hinder it.

Instrumentation should be easy as possible while still clearly documenting the intent to track a specific behavior and getting out of the way of feature changes.

## Personas that we work with

A key aspect of aligning on our direction is understanding who we are building for. This allows us to best understand the problems they may have and the context that they will be approaching our offerings with.

### Product groups within GitLab
Product [groups](https://about.gitlab.com/handbook/product/categories/) within GitLab consist of a Product Manager, Engineering Manager, engineers, and other stable counterparts. These groups implement new features in GitLab and want to understand how users interact with those features.
These teams have understanding of how the GitLab code base works as it relates to their features, but not necessarily how the instrumentation APIs and architecture work. They are not necessarily aware of the end-to-end story about how information flows from when a user clicks a button to a result being shown in Sisense or a report.

These product groups are our primary customer that we are serving and developing solutions for.

#### Product Manager personas

Consider reading more about [Parker](https://about.gitlab.com/handbook/product/personas/#parker-product-manager), our Product Manager persona.

Product Managers within these groups will have an understanding of how their group's features work from a user perspective, the problem those features solve, and what sorts of actions result in a user "succeeding" at the [job to be done](https://about.gitlab.com/handbook/product/ux/jobs-to-be-done/), and have growth and usage goals for that feature. They will be able to describe a user journey and key points that should be instrumented along that journey to measure success or need for improvement. They will not necessarily understand what the underlying code for the feature looks like or how all the technology pieces fit together. They need to be able to easily understand which kinds of tracking are available and how they are differentiated to be able to understand the resulting data.
If they find it difficult to add the instrumentation they want, they will instead rely solely on qualitiative analysis, such as direct user conversations, rather than a blend of both qualititative and quantitative analysis. 

#### Engineer personas

Consider reading more about [Sasha](https://about.gitlab.com/handbook/product/personas/#sasha-software-developer), our Software Developer persona.

Engineers within these groups will have an understanding of how GitLab is built and run but likely are not familiar with the product instrumentation architecture nor APIs. They heavily rely on documentation, examples, and previous MRs to add instrumentation that their PM requests. When they are unable to self-serve, they will ask the Analytics Instrumentation group for help or give up.

This persona relies on what we provide to them, which means it is critical for us to keep examples up to date and have clear guidance around deprecated APIs so that engineers use our newer, preferred APIs instead of older ones.

### GitLab Data program

The GitLab [data program](https://about.gitlab.com/handbook/business-technology/data-team/#data-program-teams) is responsible for surfacing data and data-driven insights to the business. They have expertise in building data pipelines, models, and managing data once collected.
They are not necessarily familiar with the GitLab code base and rely on product groups to add instrumentation for new metrics or update existing ones.
They rely on Analytics Instrumentation to effectively collect and send data to Snowflake, which is their main interface with the data.

### Customer success

### External GitLab users

External GitLab users are a broad category of individuals with different needs and who have different skill sets. These users may be interested in reading about what data we collect and how to interact with it. In the future, external users will use the application instrumentation SDKs our group provides to be able to instrument their own apps.

External users rely on our handbook pages and sites like [metrics.gitlab.com](https://metrics.gitlab.com/) to understand what data is collected from their GitLab use, how to view it, and how to interact with it. If they are unable to get clear answers to their questions, they become frustrated. In that case, they may reach out to their account manager to help them, post on a forum, or stop using GitLab.

External users will use the application instrumentation SDKs we provide to instrument their apps. These teams will be similar to our own product groups within GitLab. That is, PMs will understand user journeys about their features, developers will understand how their own app is built, but neither will be familiar with our instrumentation SDKs. They will rely heavily on our documentation and examples or else they will give up and do something else. 

## Challenges we face in Analytics Instrumentation

- GitLab's [single application approach to DevOps](/handbook/product/single-application/) creates a product that is both wide and deep, encompassing a large collection of features used by many teams within an organization, which are composed of different types of users.
- That depth/breadth makes it exceedingly complex to properly map out and understand how our diverse customer set is using the product and gaining value.
- We currently are unable to provide GitLab the required data to identify opportunities and make the right decisions against them.
- GitLab's MVC approach to product development introduces frequent changes to the product stages and what data is available, making historical trend analysis difficult.
- There are more and more supplementary applications outside of the GitLab instance, such as the [AI gateway](https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist), that need their own instrumentation setup to get a full picture of product usage.


## Short-term roadmap (As of 2024-07-09)

The roadmap for the group includes multiple initiatives that also tie-in to the stage [roadmap](https://about.gitlab.com/direction/analytics/#1-year-plan).

Our one-year strategy involves prioritizing the following key areas:

### Scaling the instrumentation system to cater to new use cases

As the group responsible for providing instrumentation interfaces for teams within GitLab, our foremost objective is guaranteeing that teams are equipped with the instrumentation interfaces essential irrespective of the use case. In the upcoming year, a key focus will be placed on [expanding product data usage tracking beyond the GitLab instance to cater to GitLab Duo use cases](https://gitlab.com/groups/gitlab-org/-/epics/13223)

### Improving feature instrumentation coverage across teams

While our interfaces support feature instrumentation, our primary goal remains advancing data-driven decision-making at GitLab. Additional instrumentation within the product is crucial for understanding user interactions, setting the stage to equip teams with actionable insights.

We'll collaborate with teams across GitLab to amplify feature instrumentation coverage, leveraging the expertise of our Program Managers to bolster this initiative. We'll also establish a tracking system for feature instrumentation, ensuring stakeholders have a dependable resource for aligning features with metrics.

In the immediate term of FY25, [our focus](https://gitlab.com/groups/gitlab-org/analytics-section/analytics-instrumentation/-/epics/6) is on increasing the utilization rates of our instrumentation interfaces and fostering collaboration to enhance feature instrumentation. Looking ahead, our vision includes supporting "Product Adoption" experiments to assist users in embracing valuable capabilities. As we progress, we'll assess our needs and identify stable counterparts to further drive progress in Product Adoption.

### Improve ease of use and efficiency of internal events
 
[Internal Event Tracking](https://docs.gitlab.com/ee/development/internal_analytics/) replaces the existing Snowplow/Redis(HLL) based approaches and provides a single unified way of instrumenting features while abstracting the underlying details from the user. In the upcoming year, our focus remains on tackling pain points identified through feedback from internal events. Our ultimate objective is to streamline instrumentation for our engineers and product managers, eliminating the necessity for resorting to alternative tracking methods beyond internal events.

In the coming months, we will also explore aligning our architecture with solutions for other analytic use-cases at GitLab in a [working group](https://gitlab.com/gitlab-com/content-sites/internal-handbook/-/merge_requests/4370).

### Support Product Analytics

[Product Analytics](https://about.gitlab.com/direction/analytics/product-analytics/) is aimed at helping both our internal and external customers understand which parts of their apps are most heavily used and how users interact with those apps. This facilitates self-service access to product usage data, empowering data-driven decision-making processes. A critical step in this is instrumenting the app and collecting the data. The Analytics Instrumentation group currently provides [five language SDKs](https://gitlab.com/groups/gitlab-org/-/epics/10535) ( Browser, Node.js, Ruby, Python and .Net) for instrumenation. In addition it also provides the system to generate, collect and store events. This year we worked on [scaling data collection](https://gitlab.com/groups/gitlab-org/-/epics/12021) in this system for bigger volumes and concurrent projects as product analytics onboards more customers. With Snowplow moving away from open source licensing, we will evaluate and migrate our data collection to an [open-source platform.](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/7409)

### Revamp product usage data architecture

This year, we will contribute towards the goals of the [Product Usage Data Architecture Working Group](https://internal.gitlab.com/handbook/company/internal-working-groups/product-usage-data-architecture/) focused on achieving a streamlined and integrated architecture to enhance our data landscape.

### Resolve service ping errors

Customer Success agents are now able to get better insights into Service Ping errors for high-revenue customers through this [report](https://gitlab.com/gitlab-data/product-analytics/-/issues/1788). However, the errors are not fully actionable without active involvement from Customer Success and the customers. In the coming year, we will work towards building [solutions](https://gitlab.com/groups/gitlab-org/-/epics/14059) to help Customer Success quickly and more efficiently resolve these errors at the earliest. Our eventual goal is to reduce Service Ping errors as a percentage of our ARR.

### Supporting the needs of the business

Different parts of the business have different needs related to data. Examples could include needing new sources of data
added to Service Ping, updating existing metrics, fixing bugs in existing systems, or other projects.  

We expect these types of requests to continue, so we will support requests from different stakeholders as they arise.

## Mid-term roadmap

In the subsequent years our key areas of focus will be:

### Cross stage data vision

The [cross stage data vision](https://about.gitlab.com/direction/monitor/cross-stage-vision.html) aims to have a single way of tracking, storing and reporting on usage data. Internal Event Tracking will eventually be able to leverage the instrumentation SDKs that are described above so that GitLab uses Product Analytics for its usage reporting, rather than external tools like Tableau or one-off spreadsheets. This plays into the cross-stage vision for data. This unified interface will eventually become the single way that data is recorded about how the Gitlab application is used. It will be the single-source of truth for all data that is collected and that reporting is done on.

### Enable and simplify customer access to GitLab analytics data 

We've noted numerous instances where customers have expressed a necessity to access their GitLab usage data, be it for monitoring purposes, validating expenditures on GitLab, or tracking feature-specific usage to enhance adoption within their teams. In version 16.9, we introduced accessibility to [Service Ping via REST API connection for our self-managed customers](https://about.gitlab.com/releases/2024/02/15/gitlab-16-9-released/#access-gitlab-usage-data-through-the-rest-api), enabling them to conveniently access service ping data. Moving forward, our focus will be to [enable GitLab.com customers to access and derive insights from usage data](https://gitlab.com/groups/gitlab-org/-/epics/12645).


## How We Work

For more information on Analytics Instrumentation, you can checkout our [Analytics Instrumentation Guide](https://about.gitlab.com/handbook/product/analytics-instrumentation-guide/) which details a [high-level overview of how we make data usable](https://about.gitlab.com/handbook/product/analytics-instrumentation-guide/#analytics-instrumentation-overview), the [Collection Frameworks](https://about.gitlab.com/handbook/product/analytics-instrumentation-guide/#collection-framework) we leverage, our [Metrics Dictionary](https://about.gitlab.com/handbook/product/analytics-instrumentation-guide/#metrics-dictionary), and much more!


## Working Groups and Cross-Functional Initiatives

Analytics Instrumentation provides the necessary frameworks, tooling, and expertise to help us build a better GitLab. Naturally we sit in the middle of many projects, initiatives and OKRs at GitLab. In order to provide clarity and realistic expectations to our stakeholders and customers we practice relentless prioritization ([per Product Principle #6](https://about.gitlab.com/handbook/product/product-principles/)), identifying what is above the line, what is below, and what is unfunded and not possible for us to action on in a given timeline.

This table lists recurring activities that are part of [working groups and cross-functional initiatives](https://about.gitlab.com/company/team/structure/working-groups/).

| Activity                                                                                                                           | Cadence       | Type | Teams Involved                                                              |
|------------------------------------------------------------------------------------------------------------------------------------|---------------|------|-----------------------------------------------------------------------------|
| [GTM Product Usage Data Working Group](https://docs.google.com/document/d/1riUXq1GdavnSWJklrebBeZnzcAl6XATyLod9tR6-AlQ/edit)       | Weekly        | Sync | Fulfillment PMs, Analytics Instrumentation, Data, Customer Success, Sales        |
| [Data & Analytics Program for R&D Teams](https://docs.google.com/document/d/1CRIGdNATvRAuBsYnhpEfOJ6C64B7j8hPAI0g5C8EdlU/edit)     | Every 2 Weeks | Sync | Fulfillment PMs, Analytics Instrumentation, Growth, Data                         |
| [Product ARR Drivers Sync](https://docs.google.com/document/d/1TxcJqOPWo4pP1S48OSMBnb4rysky8dRrRWJFflQkmlM/edit)                   | Monthly       | Sync | Customer Success, Sales, Product Leadership   
| [ClickHouse Datastore](https://about.gitlab.com/company/team/structure/working-groups/clickhouse-datastore/) | Weekly | Sync | Multiple |

## Quick Links

| Resource                                                                                                                          | Description                                               |
|-----------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------|
| [Internal Analytics Docs](https://docs.gitlab.com/ee/development/internal_analytics/)                                              | An implementation guide for Usage Ping                    |
| [Metrics Dictionary](/handbook/product/analytics-instrumentation-guide#metrics-dictionary)                                        | A SSoT for all collected metrics from Usage Ping               |
| [Privacy Policy](/privacy/)                                                                                                       | Our privacy policy outlining what data we collect and how we handle it     |
| [Implementing Product Performance Indicators](/handbook/product/analytics-instrumentation-guide#implementing-product-performance-indicators)                                   | The workflow for putting product performance indicators in place   |
| [Analytics Instrumentation Development Process](/handbook/engineering/development/analytics/analytics-instrumentation/) | The development process for the Analytics Instrumentation groups         |
| [Project resposibilities](/handbook/engineering/development/analytics/analytics-instrumentation/#responsibilities) | List of several projects that our group is the DRI for |
| [Incident Reporting](/handbook/engineering/development/analytics/analytics-instrumentation/#incidents) | Analytics instrumentation specific incident reporting process  |
