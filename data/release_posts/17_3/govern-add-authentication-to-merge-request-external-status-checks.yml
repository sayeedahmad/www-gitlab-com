---
features:
  secondary:
    - name: "Add authentication to merge request external status checks"
      available_in: [ultimate]
      gitlab_com: true
      add_ons: []
      documentation_link: 'https://docs.gitlab.com/ee/user/project/merge_requests/status_checks.html'
      image_url: '/images/17_3/status-check-hmac.png'
      reporter: g.hickman
      stage: govern
      categories:
        - Security Policy Management
      issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/433035'
      description: |
        External status checks can now be configured with HMAC (Hash-based Message Authentication Code) authentication. This will provide a more secure way to verify the authenticity of requests from GitLab to external services.
        
        When enabled for your status check, a shared secret is used to generate a unique signature for each request. The signature is sent in the `X-Gitlab-Signature` header, using SHA256 as the hash algorithm.

        - Improved Security: HMAC authentication prevents tampering with requests and ensures they come from a legitimate source.
        - Compliance: This feature is particularly valuable for regulated industries, such as banking, where security is paramount.
        - Backwards Compatibility: The feature will be optional and backwards compatible. Users can choose to enable HMAC authentication for new or existing checks, but existing external status checks will continue to function without changes.

        In a [future iteration](https://gitlab.com/gitlab-org/gitlab/-/issues/476163), GitLab plans to add an option to also verify and block HTTP requests.

