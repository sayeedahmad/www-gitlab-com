---
layout: markdown_page
title: "Product Section Direction - Fulfillment"
description: "The Fulfillment section at GitLab focuses on supporting our customers to purchase, upgrade, downgrade, and renew paid subscriptions."
canonical_path: "/direction/fulfillment/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

Last reviewed: 2024-05

## Fulfillment Section Overview

At GitLab, Fulfillment works to provide a seamless buying experience for our customers. We invest in [quote-to-cash](https://handbook.gitlab.com/handbook/company/quote-to-cash/) systems to make purchasing, activating, and managing GitLab subscriptions as easy as possible. This improves customer satisfaction and streamlines our go-to-market (GTM) processes, helping accelerate revenue growth for the company.

We welcome feedback on our initiatives. If you have any thoughts or suggestions, please feel free to create a merge request to this page and assign it to `@ofernandez2` for review, or [open a Fulfillment Meta issue](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/new).

### Mission

> Provide customers with an excellent experience by making it easy for them to purchase GitLab paid subscriptions and add-ons, provision their purchase, and manage subscription changes such as increasing seat count or renewing.

GitLab paid plans offer rich feature sets that enable customers to build software faster and more securely. For the Fulfillment section, success is to make it as easy as we can for a customer to transact with GitLab and unlock the value of these rich feature sets in our paid offerings.

We add new product offerings, make our subscription management process simpler, and work to support our customers' preferred purchasing channels and payment methods. This requires investments across all interfaces where customers conduct business with GitLab. Given the breadth of countries, organization sizes, and industries that benefit from the GitLab product, we strive to be excellent at both direct transactions via our web commerce portal or our sales team, as well as sales via [Channels and Alliances](https://handbook.gitlab.com/handbook/sales/#channels--alliances).

### Impact on GitLab's addressable market

We expand addressable market by launching new product offerings, including the FY24 addition of GitLab Duo Pro add-on and Enterprise Agile Planning. We also improve operational efficiency by providing a seamless end-to-end subscription management experience. This enables our Sales teams to spend more of their time on strategic discussions with customers. It also allows our Support and Finance teams to be more efficient.

## Recent Accomplishments

We strive to balance delivering on new business opportunities while continuing to simplify and improve our technical systems foundations. 

### Recent Pricing & Packaging Highlights

1. 2024-01 [GitLab Duo Pro add-on](/blog/2024/01/17/gitlab-duo-pro/).
2. 2023-11 [Enterprise Agile Planning add-on](/blog/2023/11/16/gitlab-enterprise-agile-planning-add-on-for-all-roles/) for GitLab Ultimate subscriptions.
3. 2023-02 [Premium tier price change](/blog/2023/03/02/gitlab-premium-update/) including automated transitional pricing for existing customers.

### Recent Customer Experience Improvements
1. 2024-04 Launched [GitLab Duo Pro](https://about.gitlab.com/gitlab-duo/#why-gitlab-duo) trials. 
1. 2024-04 Launched self-service purchase capabilities for Duo Pro via the [Customers Portal](https://customers.gitlab.com/customers/sign_in).
1. 2024-02 Revamped our [Customer Portal's subscription card](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/5317) to make it easy for subscription admins to understand their subscription and manage it. 
1. 2024-01 Enabled gitlab.com Ultimate trials on namespaces with an active Premium plan (requires sales team assistance).
1. 2023-08 Released [user caps for groups](https://docs.gitlab.com/ee/user/group/manage.html#user-cap-for-groups) to all namespaces, which helps gitlab.com customers better manage their paid subscription seats. 

## 1-year Plan

Our focus themes are: 

1. Enable pricing and packaging innovation at GitLab
2. Make it easier for customers to purchase from GitLab
3. Simplify our subscription model 
4. Streamline provisioning of subscriptions and trials
5. Improve subscription management
6. Drive internal team efficiency

Within these themes, our top priorities across all Fulfillment groups are outlined in our [quarterly OKRs](#okrs) (Non-Public). Some efforts by theme are outlined below. 

### Enable pricing and packaging innovation at GitLab

In FY24, we added new pricing and packaging options, including [GitLab Duo Pro add-on](/blog/2024/01/17/gitlab-duo-pro/) and [Enterprise Agile Planning add-on](/blog/2023/11/16/gitlab-enterprise-agile-planning-add-on-for-all-roles/). In addition, we supported pricing changes such as the [GitLab Premium price change](/blog/2023/03/02/gitlab-premium-update/) announced on 2023-03-02.

In FY25, we are working on GitLab Duo Enterprise add-on. For more details, see add-ons under [GitLab Pricing](https://about.gitlab.com/pricing/).

### Make it easier for customers to purchase from GitLab

#### Improve our self-service purchase experience

In FY25Q2, we simplified our experience by investing in a consolidated purchase path for our various offerings within our [Customer Portal](https://customers.gitlab.com/). We also launched support for [3-D Secure (3DS)](https://en.wikipedia.org/wiki/3-D_Secure) in all of our credit card purchase flows.

In FY25Q3, we will invest in UX improvements on the newly launched self-service purchase paths in the Customer Portal, streamlining the purchase experience for customers. We also plan to expand self-service renewal support for more subscription types, avoiding the need for sales team intervention.

For more details on this work, reference the [Subscription Management](/direction/fulfillment/subscription-management/). 

#### Enable channel partners and distributors to provide great selling motions

Some customers begin their GitLab journey via a partner, by transacting via a cloud provider's marketplace or via a software distributor. To improve their experience purchasing via these partners, we have worked to expand our APIs to support indirect transactions. 

In FY25Q2, we launched self-service public offers in the AWS Marketplace. Based on the success and customer feedback that we get, we plan to continue to expand on that investment. 

### Simplify our subscription model 

To remove complexity out of GitLab's [pricing](https://handbook.gitlab.com/handbook/company/pricing/) and billing, we want to tackle confusion with the seat overages model. We are planning a phased rollout of a feature that would allow customers to block any seat overage from happening in their GitLab instance or group, instead requiring seats to be purchased & available before additional users can consume a seat. 

Although we will be initially intoducing this no-overages functionality to a subset of customers, we hope to over time make it the default billing model for most customers.

### Streamline provisioning of subscriptions and trials

We are working on expanding and streamlining options for customers to trial and use new products. In FY25Q2 this included GitLab Duo Pro trials, and in future quarters will include Duo Enterprise. 

### Improve subscription management

Managing a GitLab subscription should be simple and largely automated. In order to make this a reality for all customers, we are investing in:
1. Adding the ability for multiple users to manage the same subscription in our Customer Portal [Epic](https://gitlab.com/groups/gitlab-org/-/epics/10495).
2. Making renewal emails more relevant, clear and actionable for the customers.
3. Allowing a customer to add seats via the Customer Portal and pay via invoice (not requiring a credit card for web direct add-ons).

For more details on this work, reference the [Subscription Management category direction](/direction/fulfillment/subscription-management/)

### Drive internal team efficiency

GitLab team members are passionate about delivering value to our customers. We are investing in the following initiatives to better enable them to do this: 

1. [Aligning customers.gitlab.com and Zuora billing account models](https://gitlab.com/groups/gitlab-org/-/epics/8331).
1. Making key internal system integrations more robust and resilient.
1. Investing in Fulfillment developer productivity via code refactors and new tooling deployment.
1. Enabling our support engineering team with new tooling built into our Customer Portal's admin tooling. 

As we complete these investments we will reduce the complexity of our order-to-cash systems, making it easier to innovate and deliver improvements to GitLab customers and our internal stakeholders across sales, billing, and more.

## Roadmap

Due to the [not public](https://handbook.gitlab.com/handbook/communication/confidentiality-levels/#not-public) nature of most of our projects, our product roadmap is internal. 

We have [Fulfillment FY25 Plans and Prioritization](https://gitlab.com/gitlab-com/Product/-/issues/12843) (also Not Public), that GitLab team members can reference to track all planned initiatives by theme.

### Roadmap Prioritization

To learn more about our roadmap prioritization principles and process, please see [Fulfillment Roadmap Prioritization](https://handbook.gitlab.com/handbook/product/fulfillment-guide/#fulfillment-roadmap-prioritization)

## Groups and categories

The Fulfillment section encompasses four groups and [nine categories](https://handbook.gitlab.com/handbook/product/categories/). See [GitLab categories](https://handbook.gitlab.com/handbook/product/categories/#fulfillment-section) for details on the section, stage, and groups organization, including a list of team members. 

A list of Stable Counterparts can be found in the [Engineering Fulfillment Sub-Department page](https://handbook.gitlab.com/handbook/engineering/development/fulfillment/#stable-counterparts)

## OKRs

Team members can reference our [Fulfillment FY25 Q3 OKRs](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/8268) (Internal). 

We follow the [OKR (Objective and Key Results)](https://handbook.gitlab.com/handbook/company/okrs/) framework to set and track goals quarterly.

## Performance Indicators

Fulfillment does not track Performance Indicators at this time. While we monitor performance metrics to ensure the availability, security, and robustness of our systems, and keep to our SLOs, our goals and product plans are all tracked in OKRs. 

## Recent Accomplishments and Learnings

See [Fulfillment Recap issues](https://gitlab.com/gitlab-com/Product/-/issues/?sort=updated_desc&state=closed&label_name%5B%5D=Fulfillment%20Recap&first_page_size=20) for recaps of other recent milestone accomplishments and learnings (internal when needed).

## Key Links

1. [Fulfillment Guide](https://handbook.gitlab.com/handbook/product/fulfillment-guide/): documentation around CustomersDot Admin tools and process documentation that is not part of the [core product documentation](https://docs.gitlab.com/).
2. [Dev - Fulfillment Sub Department](https://handbook.gitlab.com/handbook/engineering/development/fulfillment/): R&D team, priorities, prioritization processes, and more.
3. [Internal Handbook - Fulfillment](https://internal.gitlab.com/handbook/product/fulfillment/): documentation that can't be in the public handbook. Minimize this to only [Not Public](https://handbook.gitlab.com/handbook/communication/confidentiality-levels/#not-public) information, such as revenue-based KPIs or sensitive project documentation.
4. [GitLab Docs Subscription Documentation](https://docs.gitlab.com/ee/subscriptions/)