---
features:
  secondary:
    - name: "Rich text editor broader availability"
      available_in: [core, premium, ultimate]
      gitlab_com: true
      documentation_link: 'https://docs.gitlab.com/ee/user/rich_text_editor/'
      reporter: mmacfarlane
      stage: plan
      categories:
      - Team Planning
      - Portfolio Management
      epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/7098'
      description: |
        In GitLab 16.2, [we released](https://about.gitlab.com/releases/2023/07/22/gitlab-16-2-released/) the rich text editor as an alternative to the plain text editor. The rich text editor provides a "what you see is what you get" editing interface, and an extensible foundation for additional development. Until this release, however, the rich text editor was available only in issues, epics, and merge requests.

        With GitLab 16.9, the rich text editor is now available in:

        - [Requirements descriptions](https://gitlab.com/gitlab-org/gitlab/-/issues/407493)
        - [Vulnerability findings](https://gitlab.com/gitlab-org/gitlab/-/issues/407491)
        - [Release descriptions](https://gitlab.com/gitlab-org/gitlab/-/issues/407494)
        - [Design notes](https://gitlab.com/gitlab-org/gitlab/-/issues/407505)

        With improved access to the rich text editor, you can collaborate more efficiently and without previous Markdown experience.
