---
features:
  secondary:
  - name: "Multiple external participants for Service Desk"
    available_in: [core, premium, ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/project/service_desk/external_participants.html'
    video: 'https://www.youtube-nocookie.com/embed/eKNe7fYQCLc'
    reporter: msaleiko
    stage: monitor
    categories:
    - Service Desk
    issue_url: 'https://gitlab.com/groups/gitlab-org/-/epics/3758'
    description: |
      Sometimes there is more than one person involved in resolving a support ticket or
      the requester wants to keep colleagues up-to date on the state of the ticket.

      Now you can have a maximum of 10 external participants without a GitLab account on a
      Service Desk ticket and regular issues.
      
      External participants receive Service Desk notification emails for each public comment
      on the ticket, and their replies will appear as comments in the GitLab UI. 

      Simply use the quick actions [`/add_email`](https://docs.gitlab.com/ee/user/project/service_desk/external_participants.html#add-an-external-participant)
      and [`remove_email`](https://docs.gitlab.com/ee/user/project/service_desk/external_participants.html#add-an-external-participant)
      to add or remove external participants with a few keystrokes.

      You can also configure GitLab to
      [add all email addresses from the `Cc` header](https://docs.gitlab.com/ee/user/project/service_desk/external_participants.html#add-external-participants-from-the-cc-header)
      of the initial email to the Service Desk ticket.

      You can [tailor all Service Desk email templates to your liking](https://docs.gitlab.com/ee/user/project/service_desk/configure.html#customize-emails-sent-to-external-participants),
      using markdown, HTML, and dynamic placeholders.
      An [unsubscribe link placeholder](https://docs.gitlab.com/ee/user/project/service_desk/external_participants.html#add-an-external-participant)
      is available to make it easy for external participants to opt out of a conversation.
