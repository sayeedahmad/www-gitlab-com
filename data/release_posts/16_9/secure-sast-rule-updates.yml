---
features:
  secondary:
  - name: "Updated SAST rules for higher-quality results"
    available_in: [core, premium, ultimate]
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/sast/rules.html#important-rule-changes'
    reporter: connorgilbert
    stage: secure
    categories:
    - SAST
    epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/10971'
    description: |
      We've updated more than 40 default GitLab SAST rules to:

      - Increase true-positive results (correctly identified vulnerabilities) and reduce false-negative results (incorrectly identified vulnerabilities) by updating the detection logic rules for C#, Go, Java, JavaScript, and Python.
      - Add [OWASP mappings](https://gitlab.com/gitlab-org/gitlab/-/issues/438561) for C#, Go, Java, and Python rules.

      The rule changes are included in updated versions of the Semgrep-based GitLab SAST [analyzer](https://docs.gitlab.com/ee/user/application_security/sast/analyzers/).
      This update is automatically applied on GitLab 16.0 or newer unless you've [pinned SAST analyzers to a specific version](https://docs.gitlab.com/ee/user/application_security/sast/#pinning-to-minor-image-version).
      We're working on more SAST rule improvements in [epic 10907](https://gitlab.com/groups/gitlab-org/-/epics/10907).
