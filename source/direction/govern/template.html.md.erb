---
layout: sec_direction
title: Product Stage Direction - Govern
description: "GitLab Govern helps organizations manage security vulnerabilities, policies, compliance, and users across their enterprise. Learn more!"
canonical_path: "/direction/govern/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

<p align="center">
    <font size="+2">
        <b>Organization-wide security vulnerability, policy, compliance, and user management</b>
    </font>
</p>

<%= partial("direction/govern/templates/overview") %>

<%= devops_diagram(["Govern"]) %>

## Stage Overview

The Govern stage provides the capabilities necessary to meet security and compliance requirements for organizations at any scale, from one project to tens of thousands of projects. This includes the ability to manage policies centrally, at scale, and have them apply to projects across the organization.

### Groups

The Govern Stage is made up of six groups:

* [Anti-Abuse](/direction/govern/anti-abuse/) - Ensure that the GitLab platform itself is resistant to abusive behaviors and attacks.
* [Authentication](/direction/govern/authentication/) - Complete user lifecycle management and assurance that all points of authentication into GitLab are performed securely.
* [Authorization](/direction/govern/authorization/) - Ensure that users have roles and permissions that balance security and ease of performing their job within GitLab.
* [Compliance](/direction/govern/compliance/tactical-priorities.html) - Provide users with the tools and features necessary to achieve visibility of checks, violations and audit events across the DevSecOps lifecycle.
* [Pipeline Security](/direction/govern/pipeline_security/) - Protect access to secrets and provide verifiable evidence that artifacts were created securely.
* [Security Policies](/direction/govern/security_policies) - Apply policies to enforce scans and to require security approvals for MRs when necessary.
* [Threat Insights](/direction/govern/threat_insights/17_threat_insights_priorities.html) - Holistically view, manage, and reduce potential risks across the entire DevSecOps lifecycle.

### Resourcing and Investment

The existing team members for the Govern Stage can be found in the links below:

* [Development](https://about.gitlab.com/company/team/?department=govern-section)
* [User Experience](https://about.gitlab.com/company/team/?department=govern-ux-team)
* [Product Management](https://about.gitlab.com/company/team/?department=govern-pm-team)
* [Quality Engineering](https://about.gitlab.com/company/team/?department=sec-datascience-qe-team)

## 3 Year Stage Themes

<%= partial("direction/govern/templates/themes") %>

## 3 Year Strategy

Building on those themes, some specific capabilities that we envision developing over the next 3 years include the following:

**Anti-Abuse**

1. Adaptive user-based rate limiting based on a trust score.
1. Insider threat policies to allow organizations to monitor and reduce risk.
1. Monitoring and alert system for new URLs in a pipeline network.

**Authentication**

1. Centralized management of users and tokens at scale, including visibility into access logs and automated notifications when credentials are required to be rotated.
1. Reduced friction from authentication flows across GitLab including support for new methods of authenticating.
1. Enforcement of authentication policies to increase security while minimizing disruption to automated workflows.

**Authorization**

1. Extended support for the Principle of Least Privilege to tokens in GitLab.
1. Improved management tools for custom roles as well as support for additional custom permissions.
1. More robust logic before granting access to a resource - consider not only the role of the user, but also other attributes such as security policies, trust score, etc.

**Compliance**

1. Added support to view how projects adhere to an expanded list of compliance and regulatory requirements.
1. Support for creating customized frameworks, with related requirements and checks to meet individual organizational needs.
1. Expanded support for out-of-the-box configurations that can be used to quickly bring projects into compliance.

**Pipeline Security**

1. Support for a GitLab-native secrets manager.
1. New tools to support SLSA L3 and automate generation of SLSA L3 compliant attestations.

**Security Policies**

1. A unified, global policy management experience for all security policies, spanning across GitLab's stages, including support for policies that automate vulnerability management workflows, automate compliance, protect against pipeline abuse, and policies to protect against insider threats.
1. A more robust and intelligent policy engine that can consider multiple data sources and aide in decisioning based on your use case for security enforcement or compliance use cases.
1. Improved policy change management tools, including visibility into the scope/impact of policy changes, as well as improved visibility into the history and approvals for changes made to policies.
1. The ability to proactively notify users about newly discovered vulnerabilities in production or in the default branch.

**Threat Insights**

1. Enhanced visibility provided by proactive alerts, configurable dashboards, and customizable reports will put the right information in the right form at the right time.
1. Custom risk profiles to let your organization determine the business criticality of projects and assets. This will enable risk-based decision making to maximize return on security efforts.

In addition to these areas specific to our individual groups, we also plan to expand our use of ML and AI across all of the Govern features.

## 1 Year Plan

Over the next 12 months, the Govern stage is focused on addressing critical needs for security and compliance teams.  Some of the key initiatives include the following:

1. **Scalable vulnerability & dependency management** - Instance-wide dashboard for self managed customers, additional filtering and grouping options, automation for auto-dismissing, reporting on multiple branches beyond just the default branch.
1. **Custom role improvements** - additional permissions, better management (bulk role assignment), support for LDAP sync.
1. **Compliance standards adherence report** - added support for requirements and checks as well as support for some customization.
1. **Expanded policy enforcement options** - new types of policies, user experience improvements, and additional options for enforcement.
1. **Enterprise token management** - gitlab.com support for the Credential Inventory, additional tools for managing access logs and for rotating tokens
1. **GitLab Secrets Manager** - a GitLab-native secrets manager that can be used to keep sensitive data secure.
1. **Expanded token security** - More granular permissioning for tokens across GitLab.

In addition to adding new features, we plan to improve the reliability of our features by increasing our test coverage.  We maintain a [prioritized list of these testing priorities](/direction/govern/testing_priorities.html).

### What We're Not Doing

Although we will likely address many of these areas in the future (as described above in our [3 year strategy](/direction/govern/#3-year-strategy)), we are not planning to make progress on the following initiatives in the next 12 months:
* Attempting to build our own Security Information and Event Management (SIEM) system
* Building analytics or algorithms to auto-tune or auto-recommend policy improvements

## Key Performance Metrics

The following metrics are used to evaluate the success of the Govern stage:

* Anti-Abuse: Monthly active users are not relevant for this group. Instead success is measured in the observed abuse rate combined with the impact to paid conversion.
* Authentication **Group Monthly Active Users**: This is the total number of users in a paid SAML group.
* Authorization **Group Monthly Active Users**: The number of unique users who are assigned to a custom role.
* Compliance **Group Monthly Active Users**: This is the total number of unique users viewing the Audit Events, Compliance Dashboard, or Credential Inventory pages in the last 28 days of the given month.
* Pipeline Security: TBD
* Security Policies **Group Monthly Active Users**: This is the total number of users that have interacted with GitLab's Vulnerability Management in the last 28 days of the given month.
* Threat Insights **Group Monthly Active Users**: This is the total number of unique sessions viewing a security dashboard, pipeline security report, or expanding MR security report in the last 28 days of the given month.

Note: We do not yet have a single metric to track the success of the Govern stage as a whole.  This is being tracked in [this issue](https://gitlab.com/gitlab-data/product-analytics/-/issues/883).

## Target Audience
GitLab identifies who our DevSecOps application is built for utilizing the following categorization. We list our view of who we will support when in priority order.
* 🟩- Targeted with strong support
* 🟨- Targeted but incomplete support
* ⬜️- Not targeted but might find value

### Today
To capitalize on the opportunities listed above, the Govern Stage has features that make it useful to the following personas today.
1. 🟩  Developers / Development Teams
1. 🟩  Application Security Teams
1. 🟨️  Compliance Specialists / Manager
1. 🟨️  Legal Teams
1. 🟨  Infrastructure Security Teams

### Medium Term (1-2 years)
As we execute our [3 year strategy](#3-year-strategy), our medium term (1-2 year) goal is to provide a single DevSecOps application that enables SecOps to work collaboratively with DevOps and development to mitigate vulnerabilities in production environments.
1. 🟩  Developers / Development Teams
1. 🟩  Application Security Teams
1. 🟩  Compliance Specialists / Manager
1. 🟩  Legal Teams
1. 🟨  Infrastructure Security Teams

## Pricing

<%= partial("direction/govern/templates/pricing") %>

<%= partial("direction/categories", :locals => { :stageKey => "govern" }) %>

## Upcoming Releases

<%= direction["all"]["all"] %>

<%= partial("direction/other", :locals => { :stage => "govern" }) %>

<p align="center">
    <i><br />
    Last Reviewed: 2024-08-15<br />
    Last Updated: 2024-08-15
    </i>
</p>
