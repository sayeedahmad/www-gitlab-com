---
layout: markdown_page
title: "Group Direction - Design System"
---

- TOC
{:toc}

|                       |                               |
| -                     | -                             |
| **Stage** | [Foundations](/direction/foundations/) |
| **Maturity** | [Viable](/direction/maturity/) |
| **Last reviewed** | `2024-06-12` |

## Introduction and how you can help

Thanks for visiting this direction page on the Design System category in GitLab. This page belongs to the [Foundations Stage](https://about.gitlab.com/handbook/product/categories/#foundations-stage) and is maintained by [Jeff Tucker](https://gitlab.com/jtucker_gl).

This direction page is a work in progress, and everyone can contribute:

- Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues/?sort=weight&state=opened&label_name%5B%5D=Category%3ADesign%20System&first_page_size=20) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?state=opened&page=1&sort=start_date_desc&label_name[]=Category:Design+System).
- Please share feedback directly via email, X (formerly Twitter), or [on a video call](https://calendly.com/jtucker-gitlab/30min). If you contribute to Pajamas, the GitLab Design System, we'd especially love to hear from you.
- Learn more about the [Pajamas Design System](https://about.gitlab.com/handbook/product/ux/pajamas-design-system/) in the handbook, or go to [design.gitlab.com](https://design.gitlab.com/).

## Overview

The goal of the Design System category is to enhance efficiency and quality for product designers, engineers, and product managers by developing and maintaining an integral piece of UX, design, and frontend infrastructure.

There are two focus areas:

1. Pajamas Design System
1. Design system integration

### Pajamas Design System

The design system is a collection of resources, components, and guidelines used to make a consistent user experience in GitLab. Contributors focus on the building blocks that makes GitLab more usable, accessible, beautiful, performant, and robust. View Pajamas at [design.gitlab.com](https://design.gitlab.com/).

### Design system integration

The value of a design system is only realized when it's being used consistently and accurately in the product that consumes it. By focusing on component migrations (adoption), implementation, and tooling, the design system moves the product closer to using a single source of truth and increases our ability to make coordinated improvements.

## Strategy and themes

As the GitLab product expands to include offerings for the entire DevOps lifecycle, it's critical to provide support for teams building a cohesive experience. To serve these needs, the Design System category defines guidelines, best practices, and provides resources that inform how teams design and build products.

## 1 year plan

[**Introduce design tokens**](https://gitlab.com/groups/gitlab-org/-/epics/10238)

GitLab currently uses SCSS variables for colors, spacing, and typography to drive consistency within the GitLab UI stylesheets. These variables are manually synchronized with the Figma component library that the design team uses, which sometimes leads to them falling out of sync. This causes mismatched expectations between design and dev due to an unclear source of truth, impeding design handoff. [Design tokens](https://www.designtokens.org/) have emerged as a standard approach for managing design system configuration. We will adopt this approach within Pajamas and GitLab UI to make design handoff more efficient and to prepare the way for high-impact theming work like an improved dark mode, accessibility-focused themes, and customizable content density. We presented an update, [Getting dark mode to GA will be exciting and scary](https://www.youtube.com/watch?v=1-WyikNd-qs), in a recent UX Showcase.

[**Remove dependency on Bootstrap**](https://gitlab.com/groups/gitlab-org/-/epics/13075)

The GitLab UI component library was built on [BootstrapVue](https://bootstrap-vue.org/) to speed up initial development of our design system. As we look to introduce [design tokens](https://design.gitlab.com/product-foundations/design-tokens) into Pajamas, we have found that the [Bootstrap](https://getbootstrap.com/) and BootstrapVue libraries do not provide the flexibility we need to move forward with our rollout. We will vendor these libraries into our [`gitlab-org/gitlab-ui`](https://gitlab.com/gitlab-org/gitlab-ui) project so that we can modify them to meet our needs.

[**Improved dark mode**](https://gitlab.com/groups/gitlab-org/-/epics/2902)

Dark mode is a fan-favorite among software developers – the [original issue for dark mode](https://gitlab.com/gitlab-org/gitlab/-/issues/14531) collected nearly 1,000 positive reactions from the community. We introduced an alpha version of dark mode as a result of that work. However, we have some long-standing issues with our alpha that have kept us from driving adoption of dark mode (e.g. by respecting UA preferences for dark mode). We will revisit our current dark mode implementation once we have implemented design tokens for Pajamas and GitLab UI. Our key goal from this work will be to both improve the end-user experience for dark mode and to reduce the overhead on other GitLab teams that introduce new features.

[**Provide styles for our type scale**](https://gitlab.com/groups/gitlab-org/gitlab-services/-/epics/15)

A type scale is a carefully selected set of type size and spacing settings that provide a harmonious balance in a typographic system. A type scale helps with consistency and visual hierarchy. We have defined [our type scale in Pajamas](https://design.gitlab.com/product-foundations/type-fundamentals#type-scales), yet have not implemented these in our component libraries yet. This prevents our designers and developers from being able to readily use our type scale definitions.

**Support frontend working groups**

Several teams across GitLab are contributing to [working groups](https://handbook.gitlab.com/handbook/company/working-groups/) that impact the GitLab frontend. While the Design System group does not specifically own these projects, we are actively supporting them:

* [Include axe automated accessibility checks in GitLab UI and write tests for components](https://gitlab.com/groups/gitlab-org/-/epics/11127) to increase accessibility coverage and compliance.
* [Migrate to Tailwind CSS](https://gitlab.com/groups/gitlab-org/-/epics/10980) to improve developer productivity and UI consistency. See [our architecture blueprint](https://docs.gitlab.com/ee/architecture/blueprints/tailwindcss) for more details.

## What we are currently working on

Watch our latest kickoff video to see our plans for the current milestone.

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/videoseries?list=PL05JrBw4t0Ko0I1IZBeFBGU5FzG84TE-o" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</figure>

## What we recently completed

* 33 of GitLab groups have [achieved >95% Pajamas adoption](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/6610), with only 10 groups falling below the 95% threshold.
* We [reached a minimum of 50% dropdown component adoption per group](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/3885#note_1631721630).
* As part of the [Google summer of code](https://gitlab.com/gitlab-org/developer-relations/google-summer-of-code-2023/-/issues/5), we selected two intens who helped with component migrations over the summer.
* Add smart semgrep package and see if we can replace all 500 button components.
* 16.2 we completed  [research](https://gitlab.com/gitlab-org/ux-research/-/issues/2413) on our Pajamas Design System internal sentiment and are using this feedback to address our roadmap where we learned about how users interact with our design system. There are [5 key results](https://gitlab.com/gitlab-org/ux-research/-/issues/2413#what-did-we-learn) from the research and we will plan to address: reduce the load between 3 separate systems, address major gaps in documentation, and review the overall effort of contributions and how to make them easier.
* 16.0 we completed our latest [VPAT evaluation](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/3425) which included both the Section 508 and WCAG templates.
* In 15.8, we released [two new fonts](https://gitlab.com/groups/gitlab-org/-/epics/8972) into GitLab.
* In 15.7, we hit a milestone of having all tracked components in the pajamas adoption scanner and we now have a completed the `SCAN:SEMGREP` phase of our pajamas component spreadsheet. This means we have accurate counts to determine when we are done. Our MVC of the [Pajamas Adoption scanner](https://gitlab-org.gitlab.io/frontend/pajamas-adoption-scanner/) is also now out of "MVC" as we will look at capacity to schedule improvements to the UI.

## What is not planned right now

- Building and integrating all components across GitLab. The scope of this group is to provide guidance and governance for our design system and related tooling, and is staffed with dedicated product designers and engineers to support that. However, creating those components and implementing them throughout the application is a large effort that requires participation from every [group and category](/handbook/product/categories/).

## Roadmap

See our [roadmap in GitLab](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=start_date_asc&layout=QUARTERS&timeframe_range_type=THREE_YEARS&label_name%5B%5D=group::design%20system&label_name%5B%5D=direction&label_name%5B%5D=Category:Design+System&progress=COUNT&show_progress=true&show_milestones=false&milestones_type=GROUP&show_labels=false).

## Target audience

Internal product designers, technical writers, engineers, and product managers.
