---
features:
  secondary:
  - name: "Explore your Product Analytics data with GitLab Duo"
    available_in: [ultimate]  # Include all supported tiers
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/product_analytics/'
    image_url: '/images/16_11/PA_customviz_duo.png'
    reporter: lfarina8
    stage: monitor # Prefix this file name with stage-informative-title.yml
    categories:
      - 'Product Analytics Visualization'
    epic_url: # Multiple links are supported. Avoid linking to confidential issues.
      - 'https://gitlab.com/groups/gitlab-org/-/epics/12245'
    description: |
      [Product Analytics is now generally available](#understand-your-users-better-with-product-analytics), and this release includes a [custom visualization designer](https://docs.gitlab.com/ee/user/analytics/analytics_dashboards.html#visualization-designer). You can use it to explore your application event data, and build dashboards to help you understand your customers' usage and adoption patterns.

      In the visualization designer, you can now ask GitLab Duo to build visualizations for you by entering plain text requests, for example "Show me the count of monthly active users in 2024" or "List the top urls this week.

      GitLab Duo in Product Analytics is available as an [Experimental](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#experiment) feature.  
      
      You can help us mature this feature by providing feedback about your experience with GitLab Duo in the custom visualization designer in this [feedback issue](https://gitlab.com/gitlab-org/gitlab/-/issues/455363).
