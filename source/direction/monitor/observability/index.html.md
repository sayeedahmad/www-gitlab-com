---
layout: markdown_page
title: "Product Direction - Monitor:Observability"
description: "The Product Direction for GitLab's Observability Group."
canonical_path: "/direction/monitor/observability/"
---

- TOC
{:toc}

Content Last Reviewed: `2024-08-20`

## Overview

This page outlines the direction for the Monitor:Observability group. Feedback is welcome: you can [create a new issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/new) or reach out to this page maintainer if you have any questions or suggestions.

## Current Categories

| Category | Description | Availability | Maturity |
|----------|-------------|--------------|----------|
| [Error Tracking](https://about.gitlab.com/direction/monitor/observability/error_tracking/) | Detect, visualize and triage applications errors | GA | Minimal |
| [Distributed Tracing](https://docs.gitlab.com/ee/operations/tracing) | Track and visualize application requests across distributed services | Beta | Minimal |
| [Metrics](https://docs.gitlab.com/ee/operations/metrics.html) | Collect and visualize application performance and health metrics | Beta | Minimal |
| [Logging](https://docs.gitlab.com/ee/operations/logs) | Centralize application and infrastructure logs collection, storage and analysis | Beta  | Minimal |

## What is Observability?

Observability software enables the visualization and analysis of application performance, availability and user experience, based on the telemetry data it generates, such as traces, metrics or logs. It allows developers and operators to understand the **impact of a code or configuration change**, answering questions such as:

- "What caused this failure?"
- "What caused this change in behavior?"
- "Why do a particular user’s requests fail?"
- "What are the performance bottlenecks impacting user experience?
- "Is my latest canary deployment stable?"

Common capabilities include the configuration of dashboards and alerts for issue detection and triage, the execution of analytical queries for deeper issue investigation and root cause analysis, as well as the capability to share findings with team members. By centralizing data from multiple sources into a single, unified platform, observability tools facilitate collaboration across software development and operations teams, enabling companies to resolve issues faster and minimize downtimes.

## Vision

We aim to provide a best-in-class observability solution that enables our customers to monitor their applications and infrastructure performance directly within GitLab. This will allow organizations to further consolidate their DevOps workflows into a single platform, enhancing operational efficiency.

## Principles

We will pursue this vision with the following principles:

#### Developer-first

We are focused on solving problems for [modern development teams](https://handbook.gitlab.com/handbook/product/personas/#sasha-software-developer) first. In doing so, we immediately add value to the majority of our current users. Over time, this will allows us to provide a unique, modern approach to observability, helping companies to further "shift-left" their monitoring workflows.

#### Support Platform engineers

[Platform teams](https://handbook.gitlab.com/handbook/product/personas/#priyanka-platform-engineer) are key to building an onramp to the product. Providing them capabilities to observe and improve the performance of their software delivery platform will allow us to land our first champion users within an organization, and help expanding to development teams.

#### Cloud-Native first

Building features first for [modern](https://handbook.gitlab.com/handbook/product/product-principles/#modern-first), [cloud-native](https://handbook.gitlab.com/handbook/product/product-principles/#cloud-native-first) environment allows us to focus on where development is going, and deliver solutions that every company aspires to use eventually.

#### Working out of the box

We want developers to have immediate access to an available observability solution. That means we will ensure it is on by default, starts with convention over configuration, and is easy to use.

#### Unified Data Platform

Consolidating data into a single place facilitates collaboration and enables to surface unique insights by correlating diverse set of information, which would otherwise be overlooked if siloed. To achieve this, we aim to store all types of telemetry data that are useful for DevOps teams - from metrics, logs, traces, and errors, to [usage analytics data](https://about.gitlab.com/direction/monitor/cross-stage-vision.html) into a cohesive data platform that can span the entire DevOps process.

#### Integrated UI

The data visualization and investigation capabilities we are building are aimed to be seamlessly integrated into the platform and enhance other GitLab features and contribute to the direction for customizable dashboards in GitLab.

#### OpenTelemetry as the instrumentation standard

We are supporting [OpenTelemetry](https://opentelemetry.io/) as the primary way to instrument, generate, collect and export telemetry data. This approach aligns with GitLab's values, as it emphasizes our support for open-source industry standards. This way, we hope to contribute to improve OpenTelemetry, and that our customers will be able to do the same. We will progressively deprecate previously built solutions based on other agents or SDK.

## What has been released over the last year?

#### **16.0 (2023-05-22):** Error Tracking is now GA for SaaS

GitLab error tracking allows developers to discover and view errors generated by their application directly within GitLab, removing the need to switch between multiple tools when debugging. In this release, we are supporting both the [GitLab integrated error tracking](https://docs.gitlab.com/ee/operations/error_tracking.html#integrated-error-tracking) and the [Sentry-based](https://docs.gitlab.com/ee/operations/error_tracking.html#sentry-error-tracking) backends.

Learn more: [Release Post](https://about.gitlab.com/releases/2023/05/22/gitlab-16-0-released/#error-tracking-is-now-generally-available), [Direction page](https://about.gitlab.com/direction/monitor/observability/error_tracking/)

#### **16.2 (2023-07-22):** Distributed Tracing Experiment release

With GitLab Distributed Tracing, users can now troubleshoot application performance issues by inspecting how a request moves through different services and systems, the timing of each operation, and any errors as they occur. It is particularly useful in the context of micro-service applications, which group multiple independent services collaborating to fulfill user requests.

Telemetry data can be collected via any OpenTelemetry SDK:

* OpenTelemetry [code instrumentation is ]()[supported](https://opentelemetry.io/docs/instrumentation/) for many popular programming languages, making it very easy for users to send data to GitLab from any application.
* Collected Traces are instantly visible in GitLab, making debugging more efficient for users, and directly integrated within their development workflows.

Collected Traces can be queried and visualized via a new integrated UI, replacing previous Jaeger-based front-end:

* Simple Search UI with free text search.
* Individual traces are displayed as a waterfall diagram providing a complete view of a request distributed across different services and operations ([issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2253 "frontend: Trace Details UI")).

#### **16.7 (2023-12-21):** Distributed Tracing Beta Release

The Beta release adds the following features:

* Users can query traces up to 30 days in the past.
* [Improved Search UI](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2364 "Tracing UI beta requirements"):
  * Filter by services, operation name, and time range.
  * Autocomplete services and operation names to simplify querying ([issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2379 "Add prepopulated Period filter to tracing search bar")).
  * Scatter plot graph surfacing trace outliers such as slow traces and errors ([issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2384 "Show scatter chart on top of trace results")).
  * Continuous scrolling with progressive loading of results to accelerate browsing through large volume of data ([issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2272 "frontend: Add Infinite Scrolling to tracing list")).

Learn more: [Tracing documentation page](https://docs.gitlab.com/ee/operations/tracing)

#### **16.8 (2024-01-18):** Metrics Experiment release

With GitLab Metrics, users can collect application and infrastructure metrics, offering valuable insights for tracking performance and system health. This first release has the following features and limitations:

* Metrics can be collected from any compatible OpenTelemetry SDK or Receiver, ie. [host-level metrics](https://github.com/open-telemetry/opentelemetry-collector-contrib/blob/main/receiver/hostmetricsreceiver/README.md) (CPU/Memory usage), [Kubernetes cluster](https://opentelemetry.io/docs/kubernetes/getting-started/), [AWS metrics](https://github.com/open-telemetry/opentelemetry-collector-contrib/tree/main/receiver/awscloudwatchmetricsreceiver)<span dir="">.</span>..
* User can list collected metrics per project and visualize metrics values per dimensions as a time-series chart.
* Metrics values are visible for 24 hours in the UI.

Learn more: [Metrics documentation page](https://docs.gitlab.com/ee/operations/metrics)

#### **16.9 (2023-02-15):** Tracing UI improvements

* Visualize RED metrics charts in Trace list ([issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2582))
* Filter by trace status ([MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141784))
* Hightlight error spans in search results ([MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140794))

Learn more: see [internal release notes](https://gitlab.com/gitlab-org/opstrace/general/-/issues/186#note_1759066679)

#### **16.10 (2023-03-21):** Logs Experiment release

* Collect and retain historical logs from any source compatible with OpenTelemetry SDK: infrastructure, system or application logs in one place
* Query multiple sources together to correlate disparate events, and connect the dots between the different parts of a system when looking for the root cause of an issue.
* By combining Logs with Traces and Metrics collection, users have now a complete toolkit to monitor, troubleshoot and remediate production issues within GitLab.

Learn more: see [release notes](https://gitlab.com/gitlab-org/opstrace/general/-/issues/186#note_1791864153)

#### **16.11 (2023-04-18):** Metrics Beta release

* Run analytical queries on real-time and historical data ([epic](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2487))
* Visualize data as [time-series](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2436) and [histogram](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2574) charts
* Improved support for incomplete traces ([issue](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146803))

Learn more: see [release notes](https://gitlab.com/gitlab-org/opstrace/general/-/issues/188#note_1839897657)

#### **17.0 (2023-05-16):** Logs Beta release

* Filter Logs by attribute and time ([epic](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2646))
* Visualize Logs volume chart in Logs search results ([issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2647))
* Auto-suggest value for common Logs attributes in Logs search ([issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2782))
* Log queries URL can be copied and shared to collaborate on issue investigation ([issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2676))
* Group Metric by attributes ([issue](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/149117))

Learn more: see [release notes](https://gitlab.com/gitlab-org/opstrace/general/-/issues/197#note_1839895965)


#### **17.1 (2023-06-20):** Data Correlation

* View Logs related to a specific Trace (and vice versa) ([issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2856))
* Improved Logs search UX

Learn more: see [release notes](https://gitlab.com/gitlab-org/opstrace/general/-/issues/198#note_1839896800)

#### **17.2 (2023-07-18):** Usage Tracking

* Track observability data usage per month at the project level ([issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2829))
* Instrument Observability with usage metrics ([issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2806))
* Enabled sample logs collection in [demo project](https://gitlab.com/gitlab-org/opstrace/sandbox/otel-demo/) ([issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2630))

Learn more: see [release notes](https://gitlab.com/gitlab-org/opstrace/general/-/issues/199#note_1839896893)

#### **17.3 (2023-08-15):** Issue integration MVC

* Create and link issue from Metrics ([issue](https://gitlab.com/groups/gitlab-org/opstrace/-/epics/145))
* Various UI improvements ([timestamp formatting](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2797), [align pages layout](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2791))

Learn more: see [release notes](https://gitlab.com/gitlab-org/opstrace/general/-/issues/203#note_2063851264)

## What's coming next?

In the next 3 months (2024-08 - 2024-10), we plan to release the following:

### Issues integration: ETA 17.4 (2024-08-15)

* Create and link issues from Metrics (done), Logs and Traces ([epic](https://gitlab.com/groups/gitlab-org/opstrace/-/epics/140))

### Cloud Connector integration: ETA 17.5 (2024-10-17)

Integrating with [Cloud Connector](https://about.gitlab.com/direction/cloud-connector/) will enable self-managed customers to use observability features by storing data remotely (on gitlab.com), and querying and visualizing data seamlessly from their self-managed instance ([epic](https://gitlab.com/groups/gitlab-org/opstrace/-/epics/95)).

### Exploratory work:

During this period, we will also explore:

* [Navigation](https://gitlab.com/gitlab-org/ux-research/-/issues/2691): Understand how users expect to discover and access Observability features Observability features, to determine the optimal information architecture navigation placement within the application
* [Dashboards](https://gitlab.com/groups/gitlab-org/opstrace/-/epics/112): Visualize telemetry data in built-in or user-configurable dashboards alongside business and user-level analytics
* [Services list](https://gitlab.com/groups/gitlab-org/opstrace/-/epics/118): Provide an overview of instrumented services and their top-level metrics

## What's our 1 year plan?
    
Our goal is to achieve minimal maturity and GA for all Observability categories, so customers can further consolidate their monitoring workflows within GitLab.

While we don't expect customers to fully replace existing solutions they may use with higher level of maturity, we expect to be able to provide development teams that are currently not using any monitoring tools a basic set of capabilities available directly in GitLab. This way, they will be able to start tracking and improving their application performance and availability.

**Prioritized development work:**

* Collect and store main telemetry data types (Traces, Metrics, Logs) at enterprise scale
* Run performant and cost-efficient analytical queries for real-time, interactive exploration and analysis of this data
* Support self-managed customers via cloud connector integration

**Exploratory work:**

* Automatically discover and centralize the listing of [monitored software components](https://gitlab.com/groups/gitlab-org/opstrace/-/epics/118) (ie. services, application) and cloud infrastructure resources (ie. hosts, clusters, environments), surfacing key metrics and issues in context

* Support Deployment Tracking / Continuous Verification use cases, integrating with [Delivery](https://about.gitlab.com/direction/delivery/) capabilities - so customers can surface health and performance metrics directly within their deployments workflows in GitLab

* Support [GitLab self-monitoring](https://gitlab.com/groups/gitlab-org/opstrace/-/epics/120) use cases: what GitLab platform administrators need to troubleshoot performance issues in their software delivery platform at the application, infrastructure or pipeline level.

Learn more: [see epic](https://gitlab.com/groups/gitlab-org/opstrace/-/epics/92 "Observability Group - FY25 HQ")


## Target Audience

* Infrastructure platform teams (SREs, Infrastructure managers, Ops teams) in medium to large-scale enterprises, who are responsible for maintaining the health, performance, and reliability of software applications and their related infrastructure.
  * User personas:
    * [Allison (Application Ops/SRE)](https://handbook.gitlab.com/handbook/product/personas/#allison-application-ops)
    * [Sidney (Systems Administrator)](https://handbook.gitlab.com/handbook/product/personas/#sidney-systems-administrator)
    * [Ingrid (Infrastructure Operator)](https://handbook.gitlab.com/handbook/product/personas/#ingrid-infrastructure-operator)
    * [Rachel (Release Manager)](https://handbook.gitlab.com/handbook/product/personas/#rachel-release-manager)
* Development teams that share the on-call responsibility
  * User personas:
    * [Sasha (Software Developer)](https://handbook.gitlab.com/handbook/product/personas/#sasha-software-developer)
    * [Delaney (Development team lead](https://handbook.gitlab.com/handbook/product/personas/#delaney-development-team-lead))


## Competitive Landscape

See [competitors page](./competitors.html)

## FAQ

1. **What is Opstrace?** On Dec 14, 2021, GitLab [acquired Opstrace](https://about.gitlab.com/press/releases/2021-12-14-gitlab-acquires-opstrace-to-expand-its-devops-platform-with-open-source-observability-solution/) to accelerate GitLab's Observability roadmap. Opstrace product has been integrated into GitLab as the foundational component of our data management platform, on top of which we are now building upon additional capabilities.
2. **What is going to happen with the Opstrace name?** We are going to archive the name Opstrace as a standalone product, and instead focus on Observability features on the GitLab DevOps platform. Timeline and work that needs to be done are tracked in [this issue](https://gitlab.com/gitlab-org/opstrace/general/-/issues/91 "Archive the name Opstrace and focus on Observability in GitLab") (internal only).

